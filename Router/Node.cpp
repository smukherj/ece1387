#include "Node.h"
#include "Utils.h"
#include <cstdlib>

namespace router
{
	const Node::ID Node::ILLEGAL_ID = static_cast<Node::ID>(-1); 

	Node::Node() :
	m_type(INVALID),
	m_id(0),
	m_x(-1),
	m_y(-1),
	m_z(0)
	{

	}

	Node::~Node()
	{

	}

	bool Node::valid() const
	{
		bool ok = true;
		ok = ok && m_type > INVALID && m_type < NUM_TYPES;
		ok = ok && m_id != 0;
		ok = ok && m_x >= 0;
		ok = ok && m_y >= 0;
		ok = ok && m_z >= 0;
		return ok;
	}

	bool Node::is_wire() const
	{
		switch(m_type)
		{
			case VTRACK:
			case HTRACK: return true;
			default: break;
		}
		return false;
	}

	bool Node::is_switch() const
	{
		switch(m_type)
		{
			case SWITCH_EAST:
			case SWITCH_WEST:
			case SWITCH_NORTH:
			case SWITCH_SOUTH: return true;
			default: break;
		}
		return false;
	}

	void Node::set_type(Type t) 
	{
		anf();
		m_type = t; 
	}

    void Node::set_id(ID id) 
    {
    	anf();
    	m_id = id; 
    }

    void Node::set_x(int x) 
    {
    	anf();
    	m_x = x; 
    }

    void Node::set_y(int y) 
    {
    	anf();
    	m_y = y; 
    }

    void Node::set_z(int z) 
    {
    	anf();
    	m_z = z; 
    }

	void Node::finalize()
	{
		anf();
		rt_assert(valid());
		m_finalized = true;
	}

	void Node::anf()
	{
		//rt_assert(!m_finalized);
	}

	void Node::check_type(Type t)
	{
		rt_assert(t > INVALID && t < NUM_TYPES);
	}

	const char* Node::type_str(Type t)
	{
		switch(t)
		{
			case INVALID: return "INVALID";
			case TERMINAL: return "TERMINAL";
            case VTRACK: return "VTRACK";
            case HTRACK: return "HTRACK";
            case SWITCH_EAST: return "SWITCH_EAST";
            case SWITCH_WEST: return "SWITCH_WEST";
            case SWITCH_NORTH: return "SWITCH_NORTH";
            case SWITCH_SOUTH: return "SWITCH_SOUTH";
            default: msg::error("Invalid type %d", t); rt_die;
		}
		return NULL;
	}
}