#include "Utils.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <execinfo.h>
#include <unistd.h>
#include <string>
#include <boost/algorithm/string/predicate.hpp>

namespace
{
    int num_router_iterations_ = 320;
    utils::SWBOX_TYPE swbox_type_ = utils::UNI;
    bool enable_graphics_ = true;
    bool override_channel_width_ = false;
    int channel_width_ = -1;
    bool disable_complete_ripup_ = false;
    bool debug_dumps_ = false;
}

namespace msg
{
	void info(const char *fmt, ...)
	{
		va_list args;
    	va_start(args, fmt);
    	printf("Info: ");
    	vprintf(fmt, args);
    	printf("\n");
    	va_end(args);
	}

	void warn(const char *fmt, ...)
	{
		va_list args;
    	va_start(args, fmt);
    	printf("Warning: ");
    	vprintf(fmt, args);
    	printf("\n");
    	va_end(args);
	}

	void error(const char *fmt, ...)
	{
		va_list args;
    	va_start(args, fmt);
    	printf("Error: ");
    	vprintf(fmt, args);
    	printf("\n");
    	va_end(args);
	}

	void debug(const char *fmt, ...)
	{
		va_list args;
    	va_start(args, fmt);
    	printf("Debug: ");
    	vprintf(fmt, args);
    	printf("\n");
    	va_end(args);
	}
}

namespace utils
{
    void parse_cmd_args(int ac, char *av[])
    {
        for(int i = 0; i < ac; ++i)
        {
            std::string arg(av[i]);
            if(boost::iequals(arg, std::string("-no_graphics")))
            {
                enable_graphics_ = false;
            }
            else if(boost::iequals(arg, std::string("-swbox_type=bidir")))
            {
                swbox_type_ = BI;
            }
            else if(boost::iequals(arg, std::string("-swbox_type=unidir")))
            {
                swbox_type_ = UNI;
            }
            else if(sscanf(arg.c_str(), "-num_iterations=%d", &num_router_iterations_) == 1)
            {
                if(num_router_iterations_ <= 0)
                {
                    msg::error("Illegal router iterations '%d' requested", num_router_iterations_);
                    msg::info("   Number of iterations must be a positive non-zero integer.");
                    exit(EXIT_FAILURE);
                }
            }
            else if(sscanf(arg.c_str(), "-override_channel_width=%d", &channel_width_) == 1)
            {
                if(channel_width_ < 1)
                {
                    msg::error("Illegal channel width override '%d'. Must be a positive integer.", channel_width_);
                    exit(EXIT_FAILURE);
                }
                override_channel_width_ = true;
            }
            else if(boost::iequals(arg, std::string("-disable_complete_ripup")))
            {
                disable_complete_ripup_ = true;
            }
            else if(boost::iequals(arg, std::string("-debug_dump")))
            {
                debug_dumps_ = true;
            }
        }

        msg::info("Router will perform %d iterations", num_router_iterations_);
        if(!enable_graphics_)
        {
            msg::info("Graphics have been disabled.");
        }
        if(swbox_type_ == UNI)
        {
            msg::info("Routing graph will use unidirectional switchboxes.");
        }
        else
        {
            rt_assert(swbox_type_ == BI);
            msg::info("Routing graph will use bidirectional switchboxes.");
        }
        if(override_channel_width_)
        {
            msg::info("Channel width overridden to %d.", channel_width_);
        }
        if(disable_complete_ripup_)
        {
            msg::info("Complete rip up of nets has been DISABLED.");
        }
        if(debug_dumps_)
        {
            msg::info("Debug dumps have been turned ON.");
        }
    }

    SWBOX_TYPE get_swbox_type()
    {
        return swbox_type_;
    }

    int get_num_router_iterations()
    {
        return num_router_iterations_;
    }

    bool enable_graphics()
    {
        return enable_graphics_;
    }

    bool override_channel_width()
    {
        return override_channel_width_;
    }

    int get_channel_width()
    {
        rt_assert(override_channel_width_);
        return channel_width_;
    }

    bool disable_complete_ripup()
    {
        return disable_complete_ripup_;
    }

    bool debug_dump()
    {
        return debug_dumps_;
    }

	void die_at(const char *filename, int line)
	{
		const int TRACE_LIMIT = 20;
		void *array[TRACE_LIMIT];
		msg::error("Fatal exit at %s:%d", filename, line);
  		size_t size;

  		// get void*'s for all entries on the stack
  		size = backtrace(array, TRACE_LIMIT);
        msg::error("Begin Stack Trace:-");
  		backtrace_symbols_fd(array, size, STDOUT_FILENO);
        msg::error("End Trace");
		exit(EXIT_FAILURE);
	}
}