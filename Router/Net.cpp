#include "Net.h"
#include "Node.h"
#include "Graph.h"
#include "Utils.h"

namespace router
{
	void Net::dump_route(FILE *fh, const Graph& g)
	{
		rt_assert(routed());
		const Node& src_node = g.node(src);
		const Node& dest_node = g.node(dest);
		fprintf(fh, "Block (%d, %d) pin %d -> Block (%d, %d) pin %d (cost %llu)\n",
			src_node.x(),
			src_node.y(),
			src_node.z(),
			dest_node.x(),
			dest_node.y(),
			dest_node.z(),
			route_cost
		);
		fprintf(fh, "{\n");
		for(size_t i = 0; i < route.size(); ++i)
		{
			Node::ID id = route[i];
			const Node& node = g.node(id);
			fprintf(fh, "   %s (%d, %d, %d) \n", 
				Node::type_str(node.type()),
				node.x(),
				node.y(),
				node.z()
			);
		}
		fprintf(fh, "}\n");
	}
}