#ifndef NET_H
#define NET_H

#include <stdio.h>
#include <vector>
#include "Node.h"

namespace router
{
	class Graph;
	struct Net
	{
		Node::ID src;
		Node::ID dest;
		std::vector<Node::ID> route;
		unsigned long long route_cost;
		bool congested;

		Net() :
		src(Node::ILLEGAL_ID),
		dest(Node::ILLEGAL_ID),
		route_cost(0),
		congested(false)
		{ }

		bool routed() const { return route.size() > 0; }

		void dump_route(FILE* fh, const Graph& g);
	};

	typedef std::vector<Net> Netlist;
}

#endif /* NET_H */