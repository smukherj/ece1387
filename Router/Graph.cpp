#include "Graph.h"
#include "Utils.h"
#include <algorithm>
#include <stdio.h>

namespace router
{
	Node::ID Graph::s_last_id = 0;

	Graph::Graph()
	{
	}

	Graph::~Graph()
	{

	}

	const Node& Graph::node(Node::ID id) const
	{
		const Node& n = m_nodes.at(id - 1);
		rt_assert(n.id() == id);
		return n;
	}

	const Node& Graph::node(int x, int y, int z, Node::Type t) const
	{
		NodeIDs ids = nodes(x, y, z, t);
		rt_assert(ids.size() == 1);
		return node(ids.at(0));
	}

	Graph::NodeIDs Graph::nodes(int x, int y, Node::Type t) const
	{
		NodeIDs result;
		for(size_t i = 0; i < m_node_grid.at(x).at(y).size(); ++i)
		{
			Node::ID id = m_node_grid[x][y][i];
			const Node& n = node(id);
			if(n.type() == t)
			{
				result.push_back(id);
			}
		}
		return result;
	}

	Graph::NodeIDs Graph::nodes(int x, int y, int z, Node::Type t) const
	{
		NodeIDs result;
		for(size_t i = 0; i < m_node_grid.at(x).at(y).size(); ++i)
		{
			Node::ID id = m_node_grid[x][y][i];
			const Node& n = node(id);
			if(n.type() == t && n.z() == z)
			{
				result.push_back(id);
			}
		}
		return result;
	}

	bool Graph::has_fanout(Node::ID id) const
	{
		NodeIDsToIDsMap::const_iterator it = m_fanouts.find(id);
		return it != m_fanouts.end();
	}

	Graph::node_id_const_iterator Graph::begin_fanouts(Node::ID id) const
	{
		NodeIDsToIDsMap::const_iterator it = m_fanouts.find(id);
		rt_assert(it != m_fanouts.end());
		return it->second.begin();
	}

	Graph::node_id_const_iterator Graph::end_fanouts(Node::ID id) const
	{
		NodeIDsToIDsMap::const_iterator it = m_fanouts.find(id);
		rt_assert(it != m_fanouts.end());
		return it->second.end();
	}

	bool Graph::has_fanin(Node::ID id) const
	{
		NodeIDsToIDsMap::const_iterator it = m_fanins.find(id);
		return it != m_fanins.end();
	}

	Graph::node_id_const_iterator Graph::begin_fanins(Node::ID id) const
	{
		NodeIDsToIDsMap::const_iterator it = m_fanins.find(id);
		rt_assert(it != m_fanins.end());
		return it->second.begin();
	}

	Graph::node_id_const_iterator Graph::end_fanins(Node::ID id) const
	{
		NodeIDsToIDsMap::const_iterator it = m_fanins.find(id);
		rt_assert(it != m_fanins.end());
		return it->second.end();
	}

	void Graph::add_node(Node& n)
	{
		n.set_id(++s_last_id);
		n.finalize();
		m_nodes.push_back(n);
		m_node_grid[n.x()][n.y()].push_back(n.id());
		m_fanouts[n.id()].begin();
		m_fanins[n.id()].begin();
	}

	void Graph::add_edge(Node::ID src, Node::ID dst, bool bidir)
	{
		rt_assert(m_nodes.size() >= src);
		rt_assert(m_nodes.size() >= dst);
		
		if(std::find(m_fanins[dst].begin(), m_fanins[dst].end(), src) == m_fanins[dst].end())
		{
			m_fanins[dst].push_back(src);
		}
		
		if(std::find(m_fanouts[src].begin(), m_fanouts[src].end(), dst) == m_fanouts[src].end())
		{
			m_fanouts[src].push_back(dst);
		}

		if(bidir)
		{
			add_edge(dst, src, false);
		}
	}

	void Graph::init_grid(int nx, int ny)
	{
		m_node_grid.resize(nx);
		for(int i = 0; i < nx; ++i)
		{
			m_node_grid[i].resize(ny);
		}
	}

	void Graph::dump(const char *filename)
	{
		FILE *fh = fopen(filename, "w");
		if(!fh)
		{
			msg::warn("Could not dump routing graph. Failed to open file '%s' for writing", filename);
			return;
		}
		fprintf(fh, "begin_node_list\n");
		for(Node::ID i = 1; i <= m_nodes.size(); ++i)
		{
			const Node& n = node(i);
			fprintf(fh, "%llu %s (%d, %d, %d)\n", 
				n.id(),
				Node::type_str(n.type()),
				n.x(),
				n.y(),
				n.z()
			);
		}
		fprintf(fh, "end_node_list\n\n");
		fprintf(fh, "begin_edge_list\n");
		for(Node::ID i = 1; i <= m_nodes.size(); ++i)
		{
			if(!has_fanout(i))
			{
				continue;
			}
			fprintf(fh, "%llu ->", i);
			node_id_const_iterator it = begin_fanouts(i);
			node_id_const_iterator end = end_fanouts(i);
			for(; it != end; ++it)
			{
				fprintf(fh, " %llu", *it);
			}
			fprintf(fh, "\n");
		}
		fprintf(fh, "end_edge_list\n");
		fflush(fh);
		msg::info("Wrote routing graph to %s", filename);
	}
}
