#ifndef UTILS_H
#define UTILS_H

namespace msg
{
	void info(const char *fmt, ...);
	void warn(const char *fmt, ...);
	void error(const char *fmt, ...);
	void debug(const char *fmt, ...);
}

namespace utils
{
	enum SWBOX_TYPE
	{
		UNI,
		BI
	};

	void parse_cmd_args(int ac, char *av[]);
	SWBOX_TYPE get_swbox_type();
	int get_num_router_iterations();
	bool enable_graphics();
	bool override_channel_width();
	int get_channel_width();
	bool disable_complete_ripup();
	bool debug_dump();

	void die_at(const char *filename, int line);
}

#define rt_die {\
	utils::die_at(__FILE__, __LINE__);\
}

#define rt_assert(x) { \
	if(!(x)) {\
		msg::error("*****Assertion Failure***** '%s'", #x);\
		utils::die_at(__FILE__, __LINE__);\
	}\
}

#endif /* UTILS_H */