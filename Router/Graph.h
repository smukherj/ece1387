#ifndef GRAPH_H
#define GRAPH_H

#include <map>
#include <vector>
#include <cstddef>

#include "Node.h"

namespace router
{
	class Graph
	{
	public:
		typedef std::vector<Node::ID> NodeIDs;
		typedef NodeIDs::const_iterator node_id_const_iterator;
		typedef std::vector<Node> Nodes;
		typedef Nodes::const_iterator node_const_iterator;

		Graph();
		~Graph();

		const Node& node(Node::ID id) const;
		const Node& node(int x, int y, int z, Node::Type t) const;
		size_t num_nodes() const { return m_nodes.size(); }
		NodeIDs nodes(int x, int y, Node::Type t) const;
		NodeIDs nodes(int x, int y, int z, Node::Type t) const;
		node_const_iterator begin_nodes() const { return m_nodes.begin(); }
		node_const_iterator end_nodes() const { return m_nodes.end(); }
		bool has_fanout(Node::ID id) const;
		node_id_const_iterator begin_fanouts(Node::ID id) const;
		node_id_const_iterator end_fanouts(Node::ID id) const;
		bool has_fanin(Node::ID id) const;
		node_id_const_iterator begin_fanins(Node::ID id) const;
		node_id_const_iterator end_fanins(Node::ID id) const;

		void add_node(Node& n);
		void add_edge(Node::ID src, Node::ID dst, bool bidir=false);

		void init_grid(int nx, int ny);
		void dump(const char *filename);

	private:
		typedef std::map<Node::ID, NodeIDs> NodeIDsToIDsMap;
		typedef std::vector<std::vector<std::vector<Node::ID> > > NodeIDsGrid;

		// Store the graph as an adjacency list.
		NodeIDsToIDsMap m_fanouts;
		NodeIDsToIDsMap m_fanins;
		Nodes m_nodes;
		NodeIDsGrid m_node_grid;

		static Node::ID s_last_id;
	};
}

#endif /* GRAPH_H */
