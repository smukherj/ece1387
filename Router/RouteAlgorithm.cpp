#include "Node.h"
#include "Graph.h"
#include "Net.h"
#include "Utils.h"
#include "RouteAlgorithm.h"
#include <algorithm>
#include <list>
#include <boost/heap/binomial_heap.hpp>
#include <vector>
#include <map>
#include <set>
#include <utility>

namespace
{
	using namespace router;

	typedef unsigned long long HeapCost;
	const HeapCost MAX_HEAP_COST = static_cast<unsigned int>(-1);
	const int NEGOTIATED_CONGESTION_TRANSITION = 0;
	HeapCost CONGESTION_BASE_PENALTY = 0;
	HeapCost CONGESTION_AVOIDANCE_PENALTY = 1;


	struct RRNode
	{
		typedef std::map<Node::ID, int> RoutedPinUsageMap;

		Node::ID id;
		Node::ID prev;
		int usage;
		HeapCost base_cost;
		int num_congested_iterations;
		// Map of source pins that were routed
		// through this node to the number of times
		// a routed from this source pin was routed
		// via this node
		RoutedPinUsageMap routed_pins;

		RRNode() :
		id(Node::ILLEGAL_ID),
		prev(Node::ILLEGAL_ID),
		usage(0),
		base_cost(0),
		num_congested_iterations(0)
		{ }
	};

	struct HeapNode
	{
		Node::ID id;
		HeapCost cost;

		HeapNode() :
		id(Node::ILLEGAL_ID),
		cost(MAX_HEAP_COST)
		{ }
	};

	class HeapNodeCompare
	{
	public:
		bool operator () (const HeapNode &lhs, const HeapNode &rhs) const
		{
			return lhs.cost > rhs.cost;
		}
	};

	typedef boost::heap::binomial_heap<HeapNode, boost::heap::compare<HeapNodeCompare> > Heap;
	typedef std::map<Node::ID, Heap::handle_type> HandleMap;
	typedef std::map<Node::ID, std::vector<size_t> > PinToNetMap;
	Heap expansion_heap_;
	HandleMap heap_handle_map_;
	std::vector<RRNode> rr_nodes_;
	PinToNetMap pin_to_nets_;
	const Graph *graph_ = NULL;
	const Netlist *netlist_ = NULL;
	int router_iteration_ = -1;

	void build_rr_nodes()
	{
		rr_nodes_.resize(graph_->num_nodes() + 1);
		for(Graph::node_const_iterator it = graph_->begin_nodes(); it != graph_->end_nodes(); ++it)
		{
			const Node& node = *it;
			RRNode rr_node;
			rr_node.id = node.id();
			rr_nodes_.at(rr_node.id) = rr_node;
		}
	}

	void build_pin_to_net_map()
	{
		pin_to_nets_.clear();
		for(size_t inet = 0; inet < netlist_->size(); ++inet)
		{
			const Net& net = netlist_->at(inet);
			pin_to_nets_[net.src].push_back(inet);
		}
	}

	HeapCost get_congestion_cost(Node::ID source_pin, const RRNode& rr_node)
	{
		HeapCost congestion_penalty = 0;
		if(rr_node.usage > 0 && rr_node.routed_pins.find(source_pin) == rr_node.routed_pins.end())
		{
			congestion_penalty = CONGESTION_AVOIDANCE_PENALTY;
		}
		return congestion_penalty;
	}

	void initialize_heap(Node::ID source_pin, Node::ID dest_pin)
	{
		expansion_heap_.clear();
		heap_handle_map_.clear();
		RRNode& source_pin_rr_node = rr_nodes_.at(source_pin);
		source_pin_rr_node.prev = Node::ILLEGAL_ID;
		Graph::node_id_const_iterator it = graph_->begin_fanouts(source_pin);
		Graph::node_id_const_iterator end = graph_->end_fanouts(source_pin);
		for(; it != end; ++it)
		{
			Node::ID track_id = *it;
			const Node& node = graph_->node(track_id);
			rt_assert(node.is_wire());
			RRNode& rr_node = rr_nodes_.at(track_id);
			rr_node.prev = source_pin;
			HeapNode heap_node;
			heap_node.id = track_id;
			heap_node.cost = get_congestion_cost(source_pin, rr_node);
			heap_handle_map_[track_id] = expansion_heap_.push(heap_node);
		}
		for(size_t inet : pin_to_nets_[source_pin])
		{
			const Net& net = netlist_->at(inet);
			if(!net.routed())
			{
				continue;
			}
			if(net.src == source_pin && net.dest == dest_pin)
			{
				continue;
			}
			Node::ID prev_id = Node::ILLEGAL_ID;
			for(Node::ID id : net.route)
			{
				RRNode& rr_node = rr_nodes_.at(id);
				rr_node.prev = prev_id;
				if(heap_handle_map_.find(id) == heap_handle_map_.end())
				{
					HeapNode heap_node;
					heap_node.id = id;
					HeapCost prev_cost = 0;
					if(prev_id != Node::ILLEGAL_ID)
					{
						prev_cost = 1 + (*heap_handle_map_.at(prev_id)).cost;
					}
					heap_node.cost = get_congestion_cost(source_pin, rr_node) + prev_cost;
					heap_handle_map_[id] = expansion_heap_.push(heap_node);
				}
				prev_id = id;
			}
		}
	}

	std::set<Node::ID> get_target_tracks(Node::ID destination_pin)
	{
		std::set<Node::ID> result;
		Graph::node_id_const_iterator it = graph_->begin_fanins(destination_pin);
		Graph::node_id_const_iterator end = graph_->end_fanins(destination_pin);
		for(; it != end; ++it)
		{
			Node::ID track_id = *it;
			const Node& node = graph_->node(track_id);
			rt_assert(node.type() == Node::VTRACK || node.type() == Node::HTRACK);
			RRNode& rr_node = rr_nodes_.at(track_id);
			rt_assert(rr_node.id == track_id);
			result.insert(track_id);
			//msg::debug("Adding target track %llu", track_id);
		}
		return result;
	}

	HeapCost get_node_cost(Node::ID source_pin, const Node& node, const Node& target_node)
	{
		HeapCost cost = 0;
		HeapCost congestion_penalty = 0;
		const RRNode& rr_node = rr_nodes_.at(node.id());
		congestion_penalty = get_congestion_cost(source_pin, rr_node);
		cost = 1 + congestion_penalty + rr_node.base_cost;
		return cost;
	}

	void relax_neighbours(Node::ID source_pin, HeapNode& heap_node, const Node& target_node, const std::set<Node::ID>& visited)
	{
		Graph::node_id_const_iterator it = graph_->begin_fanouts(heap_node.id);
		Graph::node_id_const_iterator end = graph_->end_fanouts(heap_node.id);
		for(; it != end; ++it)
		{
			Node::ID fanout_id = *it;
			if(visited.find(fanout_id) != visited.end())
			{
				continue;
			}
			const Node& fanout_node = graph_->node(fanout_id);
			if(fanout_node.type() == Node::TERMINAL)
			{
				continue;
			}

			HandleMap::iterator it = heap_handle_map_.find(fanout_id);
			if(it == heap_handle_map_.end())
			{
				HeapNode fanout_heap_node;
				fanout_heap_node.id = fanout_id;
				heap_handle_map_[fanout_id] = expansion_heap_.push(fanout_heap_node);
				it = heap_handle_map_.find(fanout_id);
				rt_assert(it != heap_handle_map_.end());
			}

			HeapNode& fanout_heap_node = *(it->second);
			HeapCost cost = heap_node.cost + get_node_cost(source_pin, fanout_node, target_node);
			if(cost < fanout_heap_node.cost)
			{
				RRNode& fanout_rr_node = rr_nodes_.at(fanout_id);
				rt_assert(fanout_rr_node.id == fanout_id);
				fanout_heap_node.cost = cost;
				fanout_rr_node.prev = heap_node.id;
				// it->second is the handle to fanout_heap_node
				expansion_heap_.decrease(it->second);
			}
			
		}
	}

	bool use_rr_node(Node::ID node_id, Node::ID src)
	{
		RRNode& rr_node = rr_nodes_.at(node_id);
		if(rr_node.routed_pins.find(src) == rr_node.routed_pins.end())
		{
			rr_node.routed_pins[src] = 0;
			rr_node.usage++;
		}
		rr_node.routed_pins[src]++;

		return rr_node.usage < 2;
	}

	bool commit_route(Net& n)
	{
		bool result = true;
		const RRNode& target_pin_rr_node = rr_nodes_.at(n.dest);
		// If this assertion fails, it means the graph was disconnected
		// and we should have never reached here. The flow should have
		// errored out earlier.
		rt_assert(target_pin_rr_node.prev != Node::ILLEGAL_ID);
		std::list<Node::ID> route;

		Node::ID cur_id = target_pin_rr_node.id;
		do
		{
			//msg::debug("Trace %llu", cur_id);
			route.push_front(cur_id);
			result = use_rr_node(cur_id, n.src) && result;
			cur_id = rr_nodes_.at(cur_id).prev;
		}
		while(cur_id != Node::ILLEGAL_ID);

		rt_assert(route.front() == n.src);
		rt_assert(route.back() == n.dest);
		rt_assert(!n.routed());
		n.route.insert(n.route.begin(), route.begin(), route.end());
		n.congested = !result;
		return result;
	}

	bool route_net(Net& n)
	{
		rt_assert(n.src != n.dest);
		bool result = false;
		const Node& source_node = graph_->node(n.src);
		const Node& target_node = graph_->node(n.dest);
#if 0
		if(router_iteration_ > 0)
		{
			msg::debug("Routing %llu (%d, %d, %d) to %llu (%d, %d, %d). Cost %llu", 
				n.src,
				source_node.x(),
				source_node.y(),
				source_node.z(),
				n.dest, 
				target_node.x(),
				target_node.y(),
				target_node.z(),
				n.route_cost);
		}
#endif
		initialize_heap(n.src, n.dest);
		std::set<Node::ID> visited;
		std::set<Node::ID> target_tracks = get_target_tracks(n.dest);
		rt_assert(source_node.type() == Node::TERMINAL);
		rt_assert(target_node.type() == Node::TERMINAL);
		while(!expansion_heap_.empty())
		{
			const HeapNode& const_heap_node = expansion_heap_.top();
			rt_assert(const_heap_node.cost != MAX_HEAP_COST);
			if(target_tracks.find(const_heap_node.id) != target_tracks.end())
			{
				RRNode& target_pin_rr_node = rr_nodes_.at(n.dest);
				target_pin_rr_node.prev = const_heap_node.id;
				n.route_cost = const_heap_node.cost;
				// We have reached a target track.
				// TODO: Continue searching if current path is congested
				result = true;
				break;
			}
			visited.insert(const_heap_node.id);
			HeapNode& heap_node = *heap_handle_map_[const_heap_node.id];
			rt_assert(heap_node.id == const_heap_node.id);
			relax_neighbours(n.src, heap_node, target_node, visited);
			expansion_heap_.pop();

		}
		//msg::debug("Done routing %llu to %llu", n.src, n.dest);
		if(!result)
		{
			msg::error("Disconnected routing graph detected!");
			msg::error("Failed to route from source pin %llu to destination_pin %llu.",
				n.src,
				n.dest);
			rt_die;
		}
		result = commit_route(n);

		return result;
	}


	void unuse_rr_node(Node::ID node_id, Node::ID src)
	{
		const Node& node = graph_->node(node_id);
		if(node.type() == Node::TERMINAL)
		{
			return;
		}

		RRNode& rr_node = rr_nodes_.at(node_id);
		RRNode::RoutedPinUsageMap::iterator it = rr_node.routed_pins.find(src);
		rt_assert(it != rr_node.routed_pins.end());
		rt_assert(it->second > 0);
		rt_assert(rr_node.usage > 0);
		it->second--;
		if(it->second == 0)
		{
			rr_node.routed_pins.erase(it);
			rr_node.usage--;
		}

	}

	void rip_up_net(Net& net)
	{
		if(!net.routed())
		{
			return;
		}
		for(size_t i = 0; i < net.route.size(); ++i)
		{
			unuse_rr_node(net.route.at(i), net.src);
		}
		net.route.clear();
	}

	void report_congestion_stats(const Netlist& netlist, const std::set<int>& congested_nets)
	{
		std::set<int>::const_iterator it;
		msg::info("   Congested nets:-");
		for(it = congested_nets.begin(); it != congested_nets.end(); ++it)
		{
			int inet = *it;
			const Net& n = netlist.at(inet);
			const Node& source_pin = graph_->node(n.src);
			const Node& dest_pin = graph_->node(n.dest);
			msg::info("      (%d, %d) pin %d -> (%d, %d) pin %d", source_pin.x(),
				source_pin.y(),
				source_pin.z(),
				dest_pin.x(),
				dest_pin.y(),
				dest_pin.z()
			);
			msg::info("      Overused nodes:-");
			for(size_t inode = 0; inode < netlist.at(inet).route.size(); ++inode)
			{
				const RRNode& rr_node = rr_nodes_.at(netlist.at(inet).route.at(inode));
				if(rr_node.usage > 1)
				{
					const Node& node = graph_->node(rr_node.id);
					msg::info("         %s (%d, %d, %d)",
						Node::type_str(node.type()),
						node.x(),
						node.y(),
						node.z());
					msg::info("         Source pins:-");
					RRNode::RoutedPinUsageMap::const_iterator it;
					for(it = rr_node.routed_pins.begin(); it != rr_node.routed_pins.end(); ++it)
					{
						Node::ID routed_pin = it->first;
						const Node& routed_pin_node = graph_->node(routed_pin);
						msg::info("            (%d, %d) pin %d", routed_pin_node.x(),
							routed_pin_node.y(),
							routed_pin_node.z());
					}	
				}
			}
		}
	}

	void adjust_base_penalties()
	{
		if (graph_->num_nodes() > 350)
		{
			return;
		}
		for(size_t irr_node = 0; irr_node < rr_nodes_.size(); ++irr_node)
		{
			if(rr_nodes_[irr_node].usage > 1)
			{
				rr_nodes_[irr_node].base_cost += CONGESTION_BASE_PENALTY;
				rr_nodes_[irr_node].num_congested_iterations++;
			}
		}
	}

	void rip_up_all_nets(Netlist& netlist)
	{
		for(size_t inet = 0; inet < netlist.size(); ++inet)
		{
			netlist[inet].route.clear();
		}

		for(size_t irr_node = 0; irr_node < rr_nodes_.size(); ++irr_node)
		{
			if(rr_nodes_[irr_node].usage > 1)
			{
				rr_nodes_[irr_node].base_cost += CONGESTION_BASE_PENALTY;
				rr_nodes_[irr_node].num_congested_iterations++;
			}
			rr_nodes_[irr_node].usage = 0;
			rr_nodes_[irr_node].routed_pins.clear();
		}

	}

#if 0
	bool compare_net(const Net& lhs, const Net& rhs)
	{
		if(lhs.congested == rhs.congested)
		{
			return lhs.route_cost < rhs.route_cost;
		}
		else if(lhs.congested)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
#endif


	void cleanup()
	{
		expansion_heap_.clear();
		heap_handle_map_.clear();
		pin_to_nets_.clear();
		rr_nodes_.clear();
		graph_ = NULL;
		netlist_ = NULL;
		router_iteration_ = -1;
	}

	void check_routed_netlist(const Netlist& netlist)
	{
		std::set<Node::ID> unique_nodes;
		msg::info("Checking the routed netlist.");
		for(size_t inet = 0; inet < netlist.size(); ++inet)
		{
			const Net& net = netlist[inet];
			for(size_t irr_node = 0; irr_node < net.route.size(); ++irr_node)
			{
				const RRNode& rr_node = rr_nodes_.at(net.route[irr_node]);
				const Node& node = graph_->node(rr_node.id);
				if(node.is_wire())
				{
					unique_nodes.insert(node.id());
				}
				rt_assert(rr_node.usage == 1);
				rt_assert(rr_node.routed_pins.find(net.src) != rr_node.routed_pins.end());
				rt_assert(rr_node.routed_pins.size() == 1);
			}
		}
		msg::info("Netlist checks complete.");
		msg::info("   Routed circuit uses %zu wires.", unique_nodes.size());
	}

#if 0
	void report_nets(const Netlist& netlist)
	{
		for(const Net& net : netlist)
		{
			msg::debug("Net route size: %zu, cost: %llu, congested: %s",
				net.route.size(),
				net.route_cost,
				net.congested ? "yes" : "no");
		}
	}
#endif
}

namespace router
{
	bool run_route_algorithm(const Graph& g, Netlist& netlist)
	{
		msg::info("Beginning routing operation.");
		graph_ = &g;
		netlist_ = &netlist;
		bool result = false;

		build_rr_nodes();
		build_pin_to_net_map();
		for(router_iteration_ = 0; 
			router_iteration_ < utils::get_num_router_iterations();
			++router_iteration_)
		{
			msg::info("   Beginning routing iteration %d", router_iteration_);
			//report_nets(netlist);
			bool iteration_result = true;
			std::set<int> congested_nets;
			adjust_base_penalties();
			for(size_t inet = 0; inet < netlist.size(); ++inet)
			{
				bool net_result = true;
				rip_up_net(netlist.at(inet));
				if(!netlist.at(inet).routed())
				{
					net_result = route_net(netlist.at(inet));
				}
				if(!net_result)
				{
					congested_nets.insert(inet);
				}
				iteration_result = iteration_result && net_result;
			}
			if(iteration_result)
			{
				result = true;
				break;
			}
			if(router_iteration_ == utils::get_num_router_iterations())
			{
				report_congestion_stats(netlist, congested_nets);
			}
			else
			{
				msg::info("      %llu nets are congested", congested_nets.size());
				//std::sort(netlist.begin(), netlist.end(), compare_net);
				//build_pin_to_net_map();
			}
			if(router_iteration_ < NEGOTIATED_CONGESTION_TRANSITION &&
				!utils::disable_complete_ripup())
			{
				rip_up_all_nets(netlist);
			}

			if(router_iteration_ == 0)
			{
				CONGESTION_AVOIDANCE_PENALTY = 2;
			}
			if(router_iteration_ < 20)
			{
				CONGESTION_AVOIDANCE_PENALTY *= 1.5;
			}
			if(router_iteration_ == NEGOTIATED_CONGESTION_TRANSITION)
			{
				CONGESTION_BASE_PENALTY = 10;
			}
			else if(router_iteration_ < 60)
			{
				CONGESTION_AVOIDANCE_PENALTY *= 1.01;
			}
			else
			{
				CONGESTION_AVOIDANCE_PENALTY *= 1.001;
			}
			msg::info("      Congestion penalty is now %llu", CONGESTION_AVOIDANCE_PENALTY);
		}

		if(result)
		{
			check_routed_netlist(netlist);
		}
		cleanup();
		msg::info("Routing operation ending.");
		return result;
	}
}