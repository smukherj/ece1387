#include "Node.h"
#include "Net.h"
#include "Graph.h"
#include "Utils.h"
#include "Flow.h"
#include "RouteAlgorithm.h"
#include "graphics.h"
#include <cstdio>
#include <cstdlib>
#include <set>
#include <map>

namespace
{
	const float SWBOX_SIZE = 100;
	const float SWBOX_PAD = 100;
	const float LE_SIZE = 70;
	const float LE_OFFSET = 115;

	struct RouterOpts 
	{
		const char *circuit_filename;
		int chip_n;
		int channel_width;
	};

	RouterOpts opts;
	router::Graph g;
	router::Netlist netlist;
	std::set<router::Node::ID> highlighted_nets;
	std::map<router::Node::ID, router::Node::ID> node_usage;
	std::set<router::Node::ID> overused_nodes;

	void create_swboxes_at(int x, int y, int channel_width)
	{
		for(int itrack = 0; itrack < channel_width; ++itrack)
		{
			router::Node n;
			n.set_x(x);
			n.set_y(y);
			n.set_z(itrack);
			n.set_type(router::Node::SWITCH_EAST);
			g.add_node(n);
			n.set_type(router::Node::SWITCH_WEST);
			g.add_node(n);
			n.set_type(router::Node::SWITCH_NORTH);
			g.add_node(n);
			n.set_type(router::Node::SWITCH_SOUTH);
			g.add_node(n);
		}
	}

	void create_tracks_at(int x, int y, int channel_width, router::Node::Type t)
	{
		for(int itrack = 0; itrack < channel_width; ++itrack)
		{
			router::Node n;
			n.set_x(x);
			n.set_y(y);
			n.set_z(itrack);
			n.set_type(t);
			g.add_node(n);
		}
	}

	void create_terminals(int x, int y, int num_pins)
	{
		for(int ipin = 1; ipin <= num_pins; ++ipin)
		{
			router::Node n;
			n.set_x(x);
			n.set_y(y);
			n.set_z(ipin);
			n.set_type(router::Node::TERMINAL);
			g.add_node(n);
		}
	}

	void create_nodes(int nx, int ny, int channel_width)
	{
		for(int ix = 0; ix < nx; ++ix)
		{
			for(int iy = 0; iy < ny; ++iy)
			{
				create_swboxes_at(ix, iy, channel_width);
				if(ix < (nx - 1))
				{
					create_tracks_at(ix, iy, channel_width, router::Node::HTRACK);
				}
				if(iy < (ny - 1))
				{
					create_tracks_at(ix, iy, channel_width, router::Node::VTRACK);
				}
				if(ix < (nx - 1) && iy < (ny - 1))
				{
					create_terminals(ix, iy, 4);
				}
			}
		}
	}

	void create_swbox_track_bidir_connectivity(int x, int y, int nx, int ny, int channel_width)
	{
		for(int itrack = 0; itrack < channel_width; ++itrack)
		{
			if(x > 0)
			{
				const router::Node& switchbox_west = g.node(x, y, itrack, router::Node::SWITCH_WEST);
				const router::Node& west_htrack = g.node(x - 1, y, itrack, router::Node::HTRACK);
				g.add_edge(switchbox_west.id(), west_htrack.id(), true /* bidir */);
			}
			if(x < (nx - 1))
			{
				const router::Node& switchbox_east = g.node(x, y, itrack, router::Node::SWITCH_EAST);
				const router::Node& east_htrack = g.node(x, y, itrack, router::Node::HTRACK);
				g.add_edge(switchbox_east.id(), east_htrack.id(), true /* bidir */);
			}
			if(y > 0)
			{
				const router::Node& switchbox_south = g.node(x, y, itrack, router::Node::SWITCH_SOUTH);
				const router::Node& south_htrack = g.node(x, y - 1, itrack, router::Node::VTRACK);
				g.add_edge(switchbox_south.id(), south_htrack.id(), true /* bidir */);
			}
			if(y < (ny - 1))
			{
				const router::Node& switchbox_north = g.node(x, y, itrack, router::Node::SWITCH_NORTH);
				const router::Node& north_htrack = g.node(x, y, itrack, router::Node::VTRACK);
				g.add_edge(switchbox_north.id(), north_htrack.id(), true /* bidir */);
			}
		}
	}

	void create_swbox_track_unidir_connectivity(int x, int y, int nx, int ny, int channel_width)
	{
		for(int itrack = 0; itrack < channel_width; ++itrack)
		{
			if(x > 0)
			{
				const router::Node& switchbox_west = g.node(x, y, itrack, router::Node::SWITCH_WEST);
				const router::Node& west_htrack = g.node(x - 1, y, itrack, router::Node::HTRACK);
				if(itrack % 2 == 0)
				{
					g.add_edge(west_htrack.id(), switchbox_west.id());
				}
				else
				{
					g.add_edge(switchbox_west.id(), west_htrack.id());
				}
				
			}
			if(x < (nx - 1))
			{
				const router::Node& switchbox_east = g.node(x, y, itrack, router::Node::SWITCH_EAST);
				const router::Node& east_htrack = g.node(x, y, itrack, router::Node::HTRACK);
				if(itrack % 2 == 0)
				{
					g.add_edge(switchbox_east.id(), east_htrack.id());
				}
				else
				{
					g.add_edge(east_htrack.id(), switchbox_east.id());
				}
			}
			if(y > 0)
			{
				const router::Node& switchbox_south = g.node(x, y, itrack, router::Node::SWITCH_SOUTH);
				const router::Node& south_vtrack = g.node(x, y - 1, itrack, router::Node::VTRACK);
				if(itrack % 2 == 0)
				{
					g.add_edge(switchbox_south.id(), south_vtrack.id());
				}
				else
				{
					g.add_edge(south_vtrack.id(), switchbox_south.id());
				}
			}
			if(y < (ny - 1))
			{
				const router::Node& switchbox_north = g.node(x, y, itrack, router::Node::SWITCH_NORTH);
				const router::Node& north_vtrack = g.node(x, y, itrack, router::Node::VTRACK);
				if(itrack % 2 == 0)
				{
					g.add_edge(north_vtrack.id(), switchbox_north.id());
				}
				else
				{
					g.add_edge(switchbox_north.id(), north_vtrack.id());
				}
			}
		}
	}

	void create_swbox_internal_bidir_connectivity(int x, int y, int channel_width)
	{
		for(int itrack = 0; itrack < channel_width; ++itrack)
		{
			const router::Node& north = g.node(x, y, itrack, router::Node::SWITCH_NORTH);
			const router::Node& south = g.node(x, y, itrack, router::Node::SWITCH_SOUTH);
			const router::Node& east = g.node(x, y, itrack, router::Node::SWITCH_EAST);
			const router::Node& west = g.node(x, y, itrack, router::Node::SWITCH_WEST);
			g.add_edge(north.id(), south.id(), true /* bidir */);
			g.add_edge(north.id(), east.id(), true /* bidir */);
			g.add_edge(north.id(), west.id(), true /* bidir */);
			g.add_edge(south.id(), east.id(), true /* bidir */);
			g.add_edge(south.id(), west.id(), true /* bidir */);
			g.add_edge(east.id(), west.id(), true /* bidir */);
		}
	}

	void create_swbox_internal_unidir_connectivity(int x, int y, int channel_width)
	{
		for(int itrack = 0; itrack < channel_width; ++itrack)
		{
			const router::Node& north = g.node(x, y, itrack, router::Node::SWITCH_NORTH);
			const router::Node& south = g.node(x, y, itrack, router::Node::SWITCH_SOUTH);
			const router::Node& east = g.node(x, y, itrack, router::Node::SWITCH_EAST);
			const router::Node& west = g.node(x, y, itrack, router::Node::SWITCH_WEST);

			if(itrack % 2 == 0)
			{
				g.add_edge(north.id(), east.id());
				g.add_edge(west.id(), east.id());
				g.add_edge(north.id(), south.id());
				g.add_edge(west.id(), south.id());
				if(itrack < (channel_width - 1))
				{
					const router::Node& south_next = g.node(x, y, itrack + 1, router::Node::SWITCH_SOUTH);
					const router::Node& east_next = g.node(x, y, itrack + 1, router::Node::SWITCH_EAST);
					g.add_edge(south_next.id(), east.id());
					g.add_edge(east_next.id(), south.id());
				}
			}
			else
			{
				g.add_edge(east.id(), west.id());
				g.add_edge(south.id(), west.id());
				g.add_edge(east.id(), north.id());
				g.add_edge(south.id(), north.id());
				if(itrack > 0)
				{
					const router::Node& north_prev = g.node(x, y, itrack - 1, router::Node::SWITCH_NORTH);
					const router::Node& west_prev = g.node(x, y, itrack - 1, router::Node::SWITCH_WEST);
					g.add_edge(north_prev.id(), west.id());
					g.add_edge(west_prev.id(), north.id());
				}
			}
		}
	}

	void connect_terminals(int x, int y, int channel_width)
	{
		const router::Node& south_pin = g.node(x, y, 1, router::Node::TERMINAL);
		const router::Node& east_pin = g.node(x, y, 2, router::Node::TERMINAL);
		const router::Node& north_pin = g.node(x, y, 3, router::Node::TERMINAL);
		const router::Node& west_pin = g.node(x, y, 4, router::Node::TERMINAL);

		for(int itrack = 0; itrack < channel_width; ++itrack)
		{
			const router::Node& south_track = g.node(x, y, itrack, router::Node::HTRACK);
			const router::Node& east_track = g.node(x + 1, y, itrack, router::Node::VTRACK);
			const router::Node& north_track = g.node(x, y + 1, itrack, router::Node::HTRACK);
			const router::Node& west_track = g.node(x, y, itrack, router::Node::VTRACK);

			g.add_edge(south_pin.id(), south_track.id(), true /* bidir */);
			g.add_edge(east_pin.id(), east_track.id(), true /* bidir */);
			g.add_edge(north_pin.id(), north_track.id(), true /* bidir */);
			g.add_edge(west_pin.id(), west_track.id(), true /* bidir */);
		}
	}

	void create_connectivity(int nx, int ny, int channel_width)
	{
		for(int ix = 0; ix < nx; ++ix)
		{
			for(int iy = 0; iy < ny; ++iy)
			{
				switch(utils::get_swbox_type())
				{
				case utils::UNI:
				{
					create_swbox_track_unidir_connectivity(ix, iy, nx, ny, channel_width);
					create_swbox_internal_unidir_connectivity(ix, iy, channel_width);
					break;
				}
				case utils::BI:
				{
					create_swbox_track_bidir_connectivity(ix, iy, nx, ny, channel_width);
					create_swbox_internal_bidir_connectivity(ix, iy, channel_width);
					break;
				}
				default: msg::error("Unknown switchbox type %d", utils::get_swbox_type()); rt_die; break;
				}

				if(ix < (nx - 1) && iy < (ny - 1))
				{
					connect_terminals(ix, iy, channel_width);
				}
			}
		}
	}

	void check_graph()
	{

	}

	void build_route_graph(int n, int channel_width)
	/*
	 * 'n' is the dimension of the chip (10 x 10)
	 * 'w' is the width of each channel
	 */
	{
		rt_assert(n > 0 && channel_width > 0);
		msg::info("Building the routing graph.");
		int nx = n + 1, ny = n + 1;
		g.init_grid(nx, ny);
		
		create_nodes(nx, ny, channel_width);
		create_connectivity(nx, ny, channel_width);
		check_graph();

		msg::info("Routing graph construction complete.");
		msg::info("\tSwitchboxes form a %d by %d grid. Channel width is %d.",
			nx,
			ny,
			channel_width);
	}

	void dump_routed_netlist()
	{
		FILE *fh = fopen("routed_netlist.txt", "w");
		if(!fh)
		{
			msg::warn("Failed to open file to write routed netlist");
			return;
		}
		for(size_t inet = 0; inet < netlist.size(); ++inet)
		{
			netlist[inet].dump_route(fh, g);
		}
		fflush(fh);
		msg::info("Wrote routed netlist to routed_netlist.txt");
	}
}

void process_input_params(const char *circuit_filename)
{
	FILE *fh = fopen(circuit_filename, "r");
	if(!fh)
	{
		msg::error("Failed to open circuit file '%s' for reading.", circuit_filename);
		exit(EXIT_FAILURE);
	}
	if(fscanf(fh, "%d\n", &opts.chip_n) == 0)
	{
		msg::error("Failed to parse chip dimension from line 1 in '%s'", circuit_filename);
		exit(EXIT_FAILURE);
	}
	else if(opts.chip_n < 1)
	{
		msg::error("Invalid chip dimension '%d' at line 1 in '%s'", opts.chip_n, circuit_filename);
		exit(EXIT_FAILURE);
	}
	if(fscanf(fh, "%d\n", &opts.channel_width) == 0)
	{
		msg::error("Failed to parse channel_width from line 2 in '%s'", circuit_filename);
		exit(EXIT_FAILURE);
	}
	else if(opts.channel_width < 1)
	{
		msg::error("Invalid channel_width '%d' at line 2 in '%s'", opts.channel_width, circuit_filename);
		exit(EXIT_FAILURE);
	}

	if(utils::override_channel_width())
	{
		opts.channel_width = utils::get_channel_width();
	}

	build_route_graph(opts.chip_n, opts.channel_width);
	if(utils::debug_dump())
	{
		g.dump("rr_graph.txt");
	}
	msg::info("   Routing graph has %zu nodes", g.num_nodes());

	int src_x, src_y, src_pin, dest_x, dest_y, dest_pin;
	int line = 2;
	bool terminated = false;
	bool errors = false;
	while(fscanf(fh, "%d %d %d %d %d %d\n", &src_x, &src_y, &src_pin, &dest_x, &dest_y, &dest_pin) == 6)
	{
		++line;
		if(src_x == -1 && src_y == -1 && src_pin == -1 && dest_x == -1 && dest_y == -1 && dest_pin == -1)
		{
			terminated = true;
			break;
		}
		bool ok = true;
		ok = ok && src_x >= 0 && src_x < opts.chip_n;
		ok = ok && src_y >= 0 && src_y < opts.chip_n;
		ok = ok && dest_x >= 0 && dest_x < opts.chip_n;
		ok = ok && dest_y >= 0 && dest_y < opts.chip_n;
		ok = ok && src_pin >= 1 && src_pin <= 4;
		ok = ok && dest_pin >= 1 && dest_pin <= 4;
		if(!ok)
		{
			errors = true;
			msg::error("Invalid net on line %d in '%s'", line, circuit_filename);
		}
		else
		{
			const router::Node& src_node = g.node(src_x, src_y, src_pin, router::Node::TERMINAL);
			const router::Node& dest_node = g.node(dest_x, dest_y, dest_pin, router::Node::TERMINAL);
			router::Net n;
			n.src = src_node.id();
			n.dest = dest_node.id();
			netlist.push_back(n);
		}
	}
	if(!terminated)
	{
		msg::warn("Circuit file '%s' was not terminated properly", circuit_filename);
	}
	if(errors)
	{
		exit(EXIT_FAILURE);
	}
	msg::info("Netlist has %zu nets", netlist.size());
}

t_bound_box get_swbox_draw_bbox(const router::Node& node)
{
	int xstart = node.x() * (SWBOX_SIZE + SWBOX_PAD);
	int ystart = node.y() * (SWBOX_SIZE + SWBOX_PAD);
	int xend = xstart + SWBOX_SIZE;
	int yend = ystart + SWBOX_SIZE;
	return t_bound_box(xstart, ystart, xend, yend);
}

t_bound_box get_logic_block_draw_bbox(const router::Node& node)
{
	int xstart = node.x() * (SWBOX_SIZE + SWBOX_PAD) + LE_OFFSET;
	int ystart = node.y() * (SWBOX_SIZE + SWBOX_PAD) + LE_OFFSET;
	int xend = xstart + LE_SIZE;
	int yend = ystart + LE_SIZE;
	return t_bound_box(xstart, ystart, xend, yend);
}

t_point get_terminal_draw_pt(const router::Node& node)
{
	t_bound_box lb_bbox = get_logic_block_draw_bbox(node);
	float x = lb_bbox.left();
	float y = lb_bbox.bottom();

	if(node.z() == 1)
	{
		x += LE_SIZE * 0.25;
	}
	else if(node.z() == 2)
	{
		x += LE_SIZE;
		y += LE_SIZE * 0.25;
	}
	else if(node.z() == 3)
	{
		x += LE_SIZE * 0.75;
		y += LE_SIZE;
	}
	else if(node.z() == 4)
	{
		y += LE_SIZE * 0.75;
	}
	return t_point(x, y);
}

t_bound_box get_wire_draw_bbox(const router::Node& node)
{
	float xstart = node.x() * (SWBOX_SIZE + SWBOX_PAD);
	float ystart = node.y() * (SWBOX_SIZE + SWBOX_PAD);
	float xend = xstart;
	float yend = ystart;
	if(node.type() == router::Node::HTRACK)
	{
		xstart += SWBOX_SIZE;
		ystart += (SWBOX_SIZE / opts.channel_width) * node.z() + (SWBOX_SIZE / opts.channel_width) * 0.5;
		xend = xstart + SWBOX_PAD;
		yend = ystart;
	}
	else
	{
		ystart += SWBOX_SIZE;
		xstart += (SWBOX_SIZE / opts.channel_width) * node.z() + (SWBOX_SIZE / opts.channel_width) * 0.5;
		xend = xstart;
		yend = ystart + SWBOX_PAD;
	}
	return t_bound_box(xstart, ystart, xend, yend);
}

void draw_swbox(const router::Node& node)
{
	t_bound_box bbox = get_swbox_draw_bbox(node);
	setcolor(BLACK);
	setlinewidth(3);
	drawrect(bbox);
	setcolor(LIGHTSKYBLUE);
	fillrect(bbox);
}

void draw_logic_block(const router::Node& node)
{
	t_bound_box bbox = get_logic_block_draw_bbox(node);
	setcolor(BLACK);
	setlinewidth(3);
	drawrect(bbox);
	setcolor(KHAKI);
	fillrect(bbox);
}

void draw_wire(const router::Node& node, int color = BLACK, int thickness = 1)
{
	if(color != BLACK && overused_nodes.find(node.id()) != overused_nodes.end())
	{
		color = RED;
	}
	t_bound_box bbox = get_wire_draw_bbox(node);
	setcolor(color);
	setlinewidth(thickness);
	drawline(bbox.bottom_left(), bbox.top_right());
}

void draw_terminal(const router::Node& node)
{
	const int RADIUS = 4;
	t_point terminal_pt = get_terminal_draw_pt(node);
	setcolor(BLACK);
	fillarc(terminal_pt, RADIUS, 0, 360);
}

void draw_terminal_to_wire(const router::Node& terminal, const router::Node& wire, int color)
{
	rt_assert(terminal.type() == router::Node::TERMINAL);
	rt_assert(wire.is_wire());
	const int RADIUS = 2;

	t_point terminal_pt = get_terminal_draw_pt(terminal);
	t_bound_box wire_bbox = get_wire_draw_bbox(wire);

	setcolor(color);
	if(wire.type() == router::Node::HTRACK)
	{
		drawline(terminal_pt.x, terminal_pt.y, terminal_pt.x, wire_bbox.bottom());
		setcolor(BLUE);
		fillarc(t_point(terminal_pt.x, wire_bbox.bottom()), RADIUS, 0, 360);
	}
	else
	{
		drawline(terminal_pt.x, terminal_pt.y, wire_bbox.left(), terminal_pt.y);
		setcolor(BLUE);
		fillarc(t_point(wire_bbox.left(), terminal_pt.y), RADIUS, 0, 360);
	}
}

void draw_wire_to_terminal(const router::Node& wire, const router::Node& terminal, int color)
{
	rt_assert(terminal.type() == router::Node::TERMINAL);
	rt_assert(wire.is_wire());
	const int RADIUS = 2;

	t_bound_box wire_bbox = get_wire_draw_bbox(wire);
	t_point terminal_pt = get_terminal_draw_pt(terminal);

	setcolor(color);
	if(wire.type() == router::Node::HTRACK)
	{
		drawline(terminal_pt.x, terminal_pt.y, terminal_pt.x, wire_bbox.bottom());
		setcolor(BLUE);
		fillarc(t_point(terminal_pt.x, wire_bbox.bottom()), RADIUS, 0, 360);
	}
	else
	{
		drawline(terminal_pt.x, terminal_pt.y, wire_bbox.left(), terminal_pt.y);
		setcolor(BLUE);
		fillarc(t_point(wire_bbox.left(), terminal_pt.y), RADIUS, 0, 360);
	}
}

void draw_wire_to_wire(const router::Node& src_wire, const router::Node& dest_wire, int color)
{
	rt_assert(src_wire.is_wire());
	rt_assert(dest_wire.is_wire());

	t_bound_box src_bbox = get_wire_draw_bbox(src_wire);
	t_bound_box dest_bbox = get_wire_draw_bbox(dest_wire);

	setcolor(color);
	t_point src_pt, dest_pt;
	if(src_bbox.right() < dest_bbox.left())
	{
		if(src_bbox.top() < dest_bbox.bottom())
		{
			src_pt = src_bbox.top_right();
			dest_pt = dest_bbox.bottom_left();
		}
		else
		{
			src_pt = t_point(src_bbox.right(), src_bbox.bottom());
			dest_pt = t_point(dest_bbox.left(), dest_bbox.top());
		}
	}
	else
	{
		if(src_bbox.top() < dest_bbox.bottom())
		{
			src_pt = t_point(src_bbox.left(), src_bbox.top());
			dest_pt = t_point(dest_bbox.right(), dest_bbox.bottom());
		}
		else
		{
			src_pt = src_bbox.bottom_left();
			dest_pt = dest_bbox.top_right();
		}
	}
	drawline(src_pt, dest_pt);
}

void draw_edge(router::Node::ID src, router::Node::ID dest, int color)
{
	if(src == router::Node::ILLEGAL_ID)
	{
		return;
	}
	const router::Node& src_node = g.node(src);
	const router::Node& dest_node = g.node(dest);
	if(src_node.type() == router::Node::TERMINAL)
	{
		draw_terminal_to_wire(src_node, dest_node, color);
	}
	else if(dest_node.type() == router::Node::TERMINAL)
	{
		draw_wire_to_terminal(src_node, dest_node, color);
	}
	else
	{
		draw_wire_to_wire(src_node, dest_node, color);
	}

}

void draw_net(const router::Net& net, int color)
{
	router::Node::ID prev = router::Node::ILLEGAL_ID;
	for(router::Node::ID id : net.route)
	{
		const router::Node& node = g.node(id);
		if(node.is_switch())
		{
			continue;
		}
		if(node.is_wire())
		{
			draw_wire(node, color, 2);
		}
		draw_edge(prev, id, color);
		prev = id;
	}
}

void draw_net(const router::Node& source_pin, int color)
{
	for(const router::Net& net : netlist)
	{
		if(net.src == source_pin.id())
		{
			draw_net(net, color);
		}
	}
}

void draw_all_nets()
{
	for(const router::Net& net : netlist)
	{
		int color = LIMEGREEN;
		if(highlighted_nets.find(net.src) != highlighted_nets.end())
		{
			color = BLUE;
		}
		draw_net(net, color);
	}
}

void draw_device_callback()
{
	clearscreen();
	router::Graph::node_const_iterator it;
	for(int ix = 0; ix < (opts.chip_n + 1); ++ix)
	{
		for(int iy = 0; iy < (opts.chip_n + 1); ++iy)
		{
			const router::Node& swbox = g.node(ix, iy, 0, router::Node::SWITCH_NORTH);
			draw_swbox(swbox);
			if(ix < opts.chip_n && iy < opts.chip_n)
			{
				const router::Node& terminal = g.node(ix, iy, 1, router::Node::TERMINAL);
				draw_logic_block(terminal);
			}
		}
	}
	for(it = g.begin_nodes(); it != g.end_nodes(); ++it)
	{
		const router::Node& node = *it;
		switch(node.type())
		{
			case router::Node::TERMINAL: draw_terminal(node); break;
			case router::Node::HTRACK:
			case router::Node::VTRACK: draw_wire(node); break;
			default: break;
		}
	}
	draw_all_nets();
}

void mouse_press_callback(float xf, float yf, t_event_buttonPressed button_info)
{
	int block_x = static_cast<int>(xf) / static_cast<int>(SWBOX_SIZE + SWBOX_PAD);
	int block_y = static_cast<int>(yf) / static_cast<int>(SWBOX_SIZE + SWBOX_PAD);
	int block_xoffset = static_cast<int>(xf) % static_cast<int>(SWBOX_SIZE + SWBOX_PAD);
	int block_yoffset = static_cast<int>(yf) % static_cast<int>(SWBOX_SIZE + SWBOX_PAD);

	if(block_x < 0 || block_x >= opts.chip_n ||
		block_y < 0 || block_y >= opts.chip_n)
	{
		// No logic elements here. Out of bounds
		return;
	}

	if(block_xoffset < LE_OFFSET || block_xoffset >= (LE_OFFSET + LE_SIZE) ||
		block_yoffset < LE_OFFSET || block_yoffset >= (LE_OFFSET + LE_SIZE))
	{
		// No logic elements here. Out of bounds
		return;
	}


	for(int ipin = 1; ipin <= 4; ++ipin)
	{
		int color = BLUE;
		const router::Node& source_pin = g.node(block_x, block_y, ipin, router::Node::TERMINAL);
		if(highlighted_nets.find(source_pin.id()) != highlighted_nets.end())
		{
			color = LIMEGREEN;
			highlighted_nets.erase(source_pin.id());
		}
		else
		{
			highlighted_nets.insert(source_pin.id());
		}
		draw_net(source_pin, color);
	}

}

void mouse_move_callback(float x, float y)
{
	//msg::debug("Mouse press at (%.0f, %.0f)", x, y);
}

void refresh_screen(void (*drawscreen_fp) (void))
{
	highlighted_nets.clear();
	drawscreen_fp();
}

bool run_router_flow()
{
	bool result = router::run_route_algorithm(g, netlist);
	if(result && utils::debug_dump())
	{
		dump_routed_netlist();
	}
	return result;
}

void build_node_usage_map()
{
	for(const router::Net& net : netlist)
	{
		for(router::Node::ID id : net.route)
		{
			if(node_usage.find(id) == node_usage.end())
			{
				node_usage[id] = net.src;
			}
			else if(node_usage[id] != net.src)
			{
				overused_nodes.insert(id);
			}
		}
	}
}

void draw_routed_circuit()
{
	if(!utils::enable_graphics())
	{
		return;
	}
	build_node_usage_map();
	init_graphics("Router Results", WHITE);
	create_button("Window", "Refresh", refresh_screen);
	set_visible_world(t_bound_box(0, 0, 
		(opts.chip_n + 1) * (SWBOX_SIZE + SWBOX_PAD), 
		(opts.chip_n + 1) * (SWBOX_SIZE + SWBOX_PAD)
		)
	);
	event_loop(mouse_press_callback, mouse_move_callback, NULL, draw_device_callback);
}
