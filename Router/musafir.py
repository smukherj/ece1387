import sys
import re

NODE_RE = re.compile(r'(?P<id>\d+)\s+(?P<type>\w+)\s+\(\s*(?P<x>\d+)\s*,\s*(?P<y>\d+)\s*,\s*(?P<z>\d+)\s*\)')
node_map = {}

class Node:
	def __init__(self, node_id, node_type, x, y, z):
		self.id = int(node_id)
		self.type = node_type
		self.x = int(x)
		self.y = int(y)
		self.z = int(z)
		self.fanouts = set()
		self.fanins = set()

	def __repr__(self):
		return '%d %s (%d, %d, %d)'%(self.id,
			self.type,
			self.x,
			self.y,
			self.z)

def read_graph():
	fp = open('rr_graph.txt')
	for line in fp:
		if 'begin_node_list' in line:
			break
	for line in fp:
		if 'end_node_list' in line:
			break
		line = line.strip()
		if not line:
			continue
		match = NODE_RE.match(line)
		if match:
			node = Node(match.group('id'),
				match.group('type'),
				match.group('x'),
				match.group('y'),
				match.group('z'))
			assert(node.id not in node_map)
			node_map[node.id] = node
	for line in fp:
		if 'begin_edge_list' in line:
			break
	for line in fp:
		if 'end_edge_list' in line:
			break
		line = line.strip()
		if not line:
			continue
		line = line.split()
		assert(len(line) > 2)
		src = int(line[0])
		dests = [int(dest) for dest in line[2:]]
		for idest in dests:
			node_map[src].fanouts.add(idest)
			node_map[idest].fanins.add(src)

def display_node(node_id):
	if not node_id.isdigit():
		print 'Error: %s is not a number. A node id is a number'%node_id
		return
	node_id = int(node_id)
	if node_id not in node_map:
		print 'Error: node id %d doesn\'t exist'%node_id
		return
	node = node_map[node_id]
	print 'Info: Currently at %s. (%d fanouts. %d fanins)'%(str(node), len(node.fanouts), len(node.fanins))
	print 'Info: Fanouts:-'
	for ifanout in node.fanouts:
		print '   Info: %s'%str(node_map[ifanout])
	print 'Info: Fanins:-'
	for ifanin in node.fanins:
		print '   Info: %s'%str(node_map[ifanin])

read_graph()

while True:
	print 'Select a node id for traversal. q to quit'
	line = sys.stdin.readline().strip()
	if not line:
		continue
	if line.lower() == 'q':
		break
	display_node(line)

