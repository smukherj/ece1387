#ifndef PLACE_ALGORITHM_H
#define PLACE_ALGORITHM_H

namespace placer
{
	class Netlist;

	void solve(Netlist& netlist, int iteration);
	bool spread(Netlist& netlist, int iteration);
}

#endif /* PLACE_ALGORITHM_H */