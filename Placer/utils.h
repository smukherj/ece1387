#ifndef UTILS_H
#define UTILS_H

#include "block.h"
#include "net.h"

namespace msg
{
	void info(const char *fmt, ...);
	void warn(const char *fmt, ...);
	void error(const char *fmt, ...);
	void debug(const char *fmt, ...);
}

namespace utils
{
	void cmd_help();
	bool process_cmd_args(int ac, char *av[]);
	bool read_circuit(const char *filename, placer::Netlist& netlist);
	bool run_flow(placer::Netlist& netlist);
	void die_at(const char *filename, int line);
}

#define pl_die {\
	utils::die_at(__FILE__, __LINE__);\
}

#define pl_assert(x) { \
	if(!(x)) {\
		msg::error("*****Assertion Failure***** '%s'", #x);\
		utils::die_at(__FILE__, __LINE__);\
	}\
}

#endif /* UTILS_H */