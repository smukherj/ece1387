#ifndef BLOCK_H
#define BLOCK_H

#include <vector>
#include <map>

namespace placer
{
	class Block
	{
	public:
		typedef std::vector<int> Nets;
		typedef std::vector<int> Blocks;

		Block();
		Block(int id);
		Block(const Block& rhs);
		~Block();

		int id() const { return m_id; }
		double x() const { return m_x; }
		double y() const { return m_y; }
		const Nets& nets() const { return m_nets; }
		const Blocks& connected_blocks() const { return m_connected_blocks; }
		int connected_to(int block) const;
		bool placed() const;
		bool fixed() const;
		bool valid() const;
		bool pseudo() const;
		void anf() const;

		void refix(double x, double y);
		void fix(double x, double y);
		void pseudo_fix(double x, double y);
		void place(double x, double y);
		void unplace();
		void add_net(int net);
		void connect_to_block(int block);
		void disconnect_from(int block);
		void finalize();

	
	private:
		typedef std::map<int, int> BlockConnectedCount;

		int m_id = -1;
		double m_x = -1.0;
		double m_y = -1.0;
		Nets m_nets;
		Blocks m_connected_blocks;
		bool m_finalized = false;
		bool m_placed = false;
		bool m_fixed = false;
		bool m_pseudo = false;
		BlockConnectedCount m_connected_count;

	};
}

#endif /* BLOCK_H */