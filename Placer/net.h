#ifndef NET_H
#define NET_H

#include <vector>
#include "block.h"

namespace placer
{
	struct Net
	{
		std::vector<int> blocks;
	};

	struct Netlist
	{
		std::vector<Net> nets;
		std::vector<Block> blocks;
		double xmin = 0;
		double ymin = 0;
		double xmax = 0;
		double ymax = 0;
		double pxmin = 0;
		double pymin = 0;
		double pxmax = 0;
		double pymax = 0;
		int partition = 1;
		double pedge_fact;
	};
}

#endif /* NET_H */