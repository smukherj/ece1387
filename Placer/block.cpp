#include "block.h"
#include "utils.h"
#include <algorithm>

namespace placer
{
	Block::Block()
	{
	}

	Block::Block(int id)
	{
		pl_assert(id >= 0);
		m_id = id;
	}

	Block::Block(const Block& rhs)
	{
		m_id = rhs.m_id;
		m_x = rhs.m_x;
		m_y = rhs.m_y;
		m_nets = rhs.m_nets;
		m_connected_blocks = rhs.m_connected_blocks;
		m_finalized = rhs.m_finalized;
		m_placed = rhs.m_placed;
		m_fixed = rhs.m_fixed;
		m_pseudo = rhs.m_pseudo;
		m_connected_count = rhs.m_connected_count;
	}

	Block::~Block()
	{

	}

	int Block::connected_to(int block) const
	{
		auto it = m_connected_count.find(block);
		if(it == m_connected_count.end())
		{
			return 0;
		}
		else
		{
			return it->second;
		}
	}

	bool Block::placed() const
	{
		return m_placed;
	}

	bool Block::fixed() const
	{
		return m_fixed;
	}

	bool Block::valid() const
	{
		return m_id >= 0;
	}

	bool Block::pseudo() const
	{
		return m_pseudo;
	}

	void Block::anf() const
	{
		pl_assert(!m_finalized);
	}

	void Block::refix(double x, double y)
	{
		pl_assert(!m_placed && !m_pseudo && m_fixed);
		m_x = x;
		m_y = y;
	}

	void Block::fix(double x, double y)
	{
		pl_assert(!m_placed && !m_fixed && !m_pseudo);
		m_x = x;
		m_y = y;
		m_fixed = true;
	}

	void Block::pseudo_fix(double x, double y)
	{
		pl_assert(!m_placed && !m_fixed && !m_pseudo);
		fix(x, y);
		m_pseudo = true;
	}

	void Block::place(double x, double y)
	{
		pl_assert(!m_placed && !m_fixed && !m_pseudo);
		m_x = x;
		m_y = y;
		m_placed = true;
	}

	void Block::unplace()
	{
		pl_assert(m_placed && !m_fixed && !m_pseudo);
		m_x = -1.0;
		m_y = -1.0;
		m_placed = false;
	}

	void Block::add_net(int net)
	{
		anf();
		pl_assert(std::find(m_nets.begin(), m_nets.end(), net) == m_nets.end());
		m_nets.push_back(net);
	}

	void Block::connect_to_block(int block)
	{
		pl_assert(block > 0);
		pl_assert(block != m_id);
		if(std::find(m_connected_blocks.begin(), m_connected_blocks.end(), block) == m_connected_blocks.end())
		{
			//msg::debug("Connect %d -> %d", m_id, block);
			m_connected_blocks.push_back(block);
			pl_assert(m_connected_count.find(block) == m_connected_count.end());
			m_connected_count[block] = 1;
		}
		else
		{
			pl_assert(m_connected_count.find(block) != m_connected_count.end());
			m_connected_count[block]++;
		}
	}

	void Block::disconnect_from(int block)
	{
		pl_assert(block > 0);
		pl_assert(block != m_id);
		auto it = std::find(m_connected_blocks.begin(), m_connected_blocks.end(), block);
		pl_assert(it != m_connected_blocks.end());
		m_connected_blocks.erase(it);

		auto count_it = m_connected_count.find(block);
		pl_assert(count_it != m_connected_count.end());
		m_connected_count.erase(count_it);
	}

	void Block::finalize()
	{
		anf();
		std::sort(m_connected_blocks.begin(), m_connected_blocks.end());
		m_nets.clear();
		m_finalized = true;
	}
}