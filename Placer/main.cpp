#include "utils.h"

int main (int argc, char *argv[])
{
    if(argc < 2)
    {
        msg::error("Must give atleast one command line argument pointing to circuit file.");
        utils::cmd_help();
        return -1;
    }
    bool result = true;
    msg::info("*******************************************************");
    msg::info("Running Analytic Placer");
    msg::info("Compiled on %s, %s", __DATE__, __TIME__);
    placer::Netlist netlist;
    result = result && utils::process_cmd_args(argc - 2, argv + 2);
    result = result && utils::read_circuit(argv[1], netlist);
    result = result && utils::run_flow(netlist);
    if(result)
    {
        msg::info("Analytic placer was successful.");
        return 0;
    }
    else
    {
        msg::error("Analytic placer was unsuccessful.");
        return -1;
    }
}

