#include "utils.h"
#include "place_algorithm.h"
#include "net.h"
#include "block.h"
#include "umfpack.h"
#include <map>
#include <set>
#include <algorithm>
#include <iostream>
#include <cmath>

namespace
{
	using namespace placer;
	
	struct IntPair
	{
		int i = -1;
		int j = -1;
		double payload = 0.0;
	};

	struct PlaceBin
	{
		int pseudo_block_id;
		double x;
		double y;
		std::vector<int> blocks;
	};

	typedef std::map<int, int> BlockIDMap;
	typedef std::vector<placer::Block*> PlaceBlocks;
	typedef std::vector<IntPair> SparseMat;
	typedef std::vector<double> Placement1D;
	typedef std::vector<PlaceBin> PlaceBins;

	BlockIDMap block_id_map_;
	PlaceBlocks place_blocks_;
	Netlist *netlist_ = nullptr;
	PlaceBins place_bins_;

	void init_solve_global_data()
	{
		int last_block = -1;
		for(size_t iblock = 0; iblock < netlist_->blocks.size(); ++iblock)
		{
			if(!netlist_->blocks[iblock].valid() || netlist_->blocks[iblock].fixed())
			{
				continue;
			}
			Block *block = &netlist_->blocks[iblock];
			pl_assert(!block->placed());
			pl_assert(block_id_map_.find(block->id()) == block_id_map_.end());
			last_block++;
			block_id_map_[block->id()] = last_block;
			place_blocks_.push_back(block);
			pl_assert(place_blocks_.at(last_block) == block);
			//msg::debug("%d -> %d", block->id(), last_block);
		}
	}

	bool column_major_comp(const IntPair& lhs, const IntPair& rhs)
	{
		if(lhs.j != rhs.j)
		{
			return lhs.j < rhs.j;
		}
		else
		{
			return lhs.i < rhs.i;
		}
	}

	void create_mat(SparseMat& mat, const double EDGE_WEIGHT)
	{
		for(size_t i = 0; i < place_blocks_.size(); ++i)
		{
			for(size_t j = 0; j < place_blocks_.size(); ++j)
			{
				IntPair p;
				p.i = i;
				p.j = j;
				p.payload = 0.0;
				if(i == j)
				{
					const placer::Block::Blocks& connected_blocks = place_blocks_[i]->connected_blocks();
					for(int block_id : connected_blocks)
					{
						const placer::Block& connected_block = netlist_->blocks.at(block_id);

						if(connected_block.pseudo())
						{
							p.payload += (EDGE_WEIGHT * netlist_->pedge_fact);
						}
						else
						{
							int num_connections = place_blocks_[i]->connected_to(block_id);
							pl_assert(num_connections > 0);
							p.payload += (EDGE_WEIGHT * num_connections);
						}
					}
					mat.push_back(p);
				}
				else if(place_blocks_[i]->connected_to(place_blocks_[j]->id()) > 0)
				{
					p.payload = -EDGE_WEIGHT * place_blocks_[i]->connected_to(place_blocks_[j]->id());
					mat.push_back(p);
				}
#ifdef VMAT_DUMP
				printf("%.2f ", p.payload);
#endif
			}
#ifdef VMAT_DUMP
			printf("\n");
#endif
		}
		std::sort(mat.begin(), mat.end(), column_major_comp);
	}
#ifdef VMAT_DUMP
	template <typename T> void
	dump_vect(T v)
	{
		for(const auto& elem : v)
		{
			std::cout << elem << " ";
		}
		std::cout << "\n";
	}
#endif

	void convert_to_umfpack_mat(const SparseMat& mat, 
		std::vector<int>& Apv, 
		std::vector<int>& Aiv, 
		std::vector<double>& Axv)
	{
		pl_assert(!mat.empty());
		Apv.clear();
		Aiv.clear();
		Axv.clear();
		
		int cur_col = mat[0].j;
		int num_elements = 1;
		Apv.push_back(0);
		Aiv.push_back(mat[0].i);
		Axv.push_back(mat[0].payload);

		for(size_t i = 1; i < mat.size(); ++i)
		{
			const IntPair& ip = mat[i];
			Aiv.push_back(ip.i);
			Axv.push_back(ip.payload);
			if(ip.j != cur_col)
			{
				cur_col = ip.j;
				Apv.push_back(num_elements);
			}
			++num_elements;
		}
		Apv.push_back(num_elements);

		pl_assert(Apv.size() == (place_blocks_.size() + 1));
	}

	void create_const_vector(std::vector<double>& bv, const double EDGE_WEIGHT, bool x)
	{
		for(size_t i = 0; i < bv.size(); ++i)
		{
			const Block *block = place_blocks_.at(i);
			const placer::Block::Blocks& connected_blocks = block->connected_blocks();
			double b = 0.0;
			for(size_t iblock = 0; iblock < connected_blocks.size(); ++iblock)
			{
				int block_id = connected_blocks[iblock];
				if(block_id_map_.find(block_id) == block_id_map_.end())
				{
					const placer::Block& connected_block = netlist_->blocks.at(block_id);
					int num_connections = block->connected_to(block_id);
					pl_assert(num_connections > 0);
					pl_assert(connected_block.fixed());
					if(x)
					{
						if(connected_block.pseudo())
						{
							b += EDGE_WEIGHT * netlist_->pedge_fact * connected_block.x() * num_connections;
						}
						else
						{
							b += EDGE_WEIGHT * connected_block.x() * num_connections;
						}
					}
					else
					{
						if(connected_block.pseudo())
						{
							b += EDGE_WEIGHT * netlist_->pedge_fact * connected_block.y() * num_connections;
						}
						else
						{
							b += EDGE_WEIGHT * connected_block.y() * num_connections;
						}
					}
				}
			}
			bv.at(i) = b;
		}
	}

	void solve_x_coord(Placement1D& px)
	{
		const double EDGE_WEIGHT = 2.0 / place_blocks_.size(); // 2/P
		SparseMat mat;
		create_mat(mat, EDGE_WEIGHT);
		std::vector<int> Apv, Aiv;
		std::vector<double> Axv;
		std::vector<double> bv;
		bv.resize(place_blocks_.size());
		px.resize(place_blocks_.size());
		create_const_vector(bv, EDGE_WEIGHT, true);
		convert_to_umfpack_mat(mat, Apv, Aiv, Axv);
#ifdef VMAT_DUMP
		std::cout << "========= X Vectors ==========\n";
		std::cout << "Ap[" << Apv.size() << "] = ";
		dump_vect(Apv);
		std::cout << "Ai[" << Aiv.size() << "] = ";
		dump_vect(Aiv);
		std::cout << "Ax[" << Axv.size() << "] = ";
		dump_vect(Axv);
		std::cout << "bv[" << bv.size() << "] = ";
		dump_vect(bv);
#endif
		void *Symbolic, *Numeric;
		umfpack_di_symbolic(place_blocks_.size(), 
			place_blocks_.size(), 
			&Apv[0], 
			&Aiv[0], 
			&Axv[0], 
			&Symbolic, 
			nullptr, 
			nullptr);
		umfpack_di_numeric(&Apv[0], 
			&Aiv[0], 
			&Axv[0], 
			Symbolic, 
			&Numeric, 
			nullptr, 
			nullptr);
		umfpack_di_free_symbolic(&Symbolic);
		umfpack_di_solve(UMFPACK_A, 
			&Apv[0], 
			&Aiv[0], 
			&Axv[0], 
			&px[0], 
			&bv[0], 
			Numeric, 
			nullptr, 
			nullptr);
		umfpack_di_free_numeric(&Numeric);
#ifdef VMAT_DUMP
		std::cout << "px[" << px.size() << "] = ";
		dump_vect(px);
#endif
		
	}

	void solve_y_coord(Placement1D& py)
	{
		const double EDGE_WEIGHT = 2.0 / place_blocks_.size(); // 2/P
		SparseMat mat;
		create_mat(mat, EDGE_WEIGHT);
		std::vector<int> Apv, Aiv;
		std::vector<double> Axv;
		std::vector<double> bv;
		bv.resize(place_blocks_.size());
		py.resize(place_blocks_.size());
		create_const_vector(bv, EDGE_WEIGHT, false);
		convert_to_umfpack_mat(mat, Apv, Aiv, Axv);
#ifdef VMAT_DUMP
		std::cout << "========= Y Vectors ==========\n";
		std::cout << "Ap[" << Apv.size() << "] = ";
		dump_vect(Apv);
		std::cout << "Ai[" << Aiv.size() << "] = ";
		dump_vect(Aiv);
		std::cout << "Ax[" << Axv.size() << "] = ";
		dump_vect(Axv);
		std::cout << "bv[" << bv.size() << "] = ";
		dump_vect(bv);
#endif
		void *Symbolic, *Numeric;
		umfpack_di_symbolic(place_blocks_.size(), 
			place_blocks_.size(), 
			&Apv[0], 
			&Aiv[0], 
			&Axv[0], 
			&Symbolic, 
			nullptr, 
			nullptr);
		umfpack_di_numeric(&Apv[0], 
			&Aiv[0], 
			&Axv[0], 
			Symbolic, 
			&Numeric, 
			nullptr, 
			nullptr);
		umfpack_di_free_symbolic(&Symbolic);
		umfpack_di_solve(UMFPACK_A, 
			&Apv[0], 
			&Aiv[0], 
			&Axv[0], 
			&py[0], 
			&bv[0], 
			Numeric, 
			nullptr, 
			nullptr);
		umfpack_di_free_numeric(&Numeric);
#ifdef VMAT_DUMP
		std::cout << "py[" << py.size() << "] = ";
		dump_vect(py);
#endif
	}

	void disconnect_old_pseudo_blocks()
	{
		std::set<int> pseudo_blocks;
		for(size_t iblock = 0; iblock < netlist_->blocks.size(); ++iblock)
		{
			const Block& b = netlist_->blocks[iblock];
			if(!b.valid())
			{
				continue;
			}
			pl_assert(b.id() == static_cast<int>(iblock));
			if(b.pseudo())
			{
				pseudo_blocks.insert(b.id());
			}
		}
		size_t num_deleted = 0;
		while(!netlist_->blocks.empty() && netlist_->blocks.back().pseudo())
		{
			auto it = netlist_->blocks.begin() + (netlist_->blocks.size() - 1);
			netlist_->blocks.erase(it);
			++num_deleted;
		}
		pl_assert(num_deleted == pseudo_blocks.size());
		for(size_t iblock = 0; iblock < netlist_->blocks.size(); ++iblock)
		{
			Block& b = netlist_->blocks[iblock];
			if(!b.valid())
			{
				continue;
			}
			pl_assert(b.id() == static_cast<int>(iblock));
			pl_assert(!b.pseudo());
			std::vector<int> blocks_to_delete;
			for(int block_id : b.connected_blocks())
			{
				if(pseudo_blocks.find(block_id) != pseudo_blocks.end())
				{
					blocks_to_delete.push_back(block_id);
				}
			}
			for(int block_id : blocks_to_delete)
			{
				b.disconnect_from(block_id);
			}
		}
		//msg::info("   Removed %zu old pseudo blocks.", pseudo_blocks.size());
	}

	void create_new_bins(int partition, double bin_x, double bin_y, PlaceBins& bins)
	{
		double pwidth = (netlist_->pxmax - netlist_->pxmin) / partition;
		double pheight = (netlist_->pymax - netlist_->pymin) / partition;
		//msg::info("   pwidth %.4f, pheight %.4f",
			//pwidth, pheight);
		for(int i = 0; i < partition; ++i)
		{
			for(int j = 0; j < partition; ++j)
			{
				int pseudo_block_id = static_cast<int>(netlist_->blocks.size());
				Block b(pseudo_block_id);
				double x = (i + 0.5) * bin_x;
				double y = (j + 0.5) * bin_y;
				b.pseudo_fix(x, y);
				double px = (i + 0.5) * pwidth + netlist_->pxmin;
				double py = (j + 0.5) * pheight + netlist_->pymin;
				//msg::debug("      Created bin %d at (%.2f, %.2f), placement bin at (%.4f, %.4f).",
					//pseudo_block_id, x, y, px, py);
				netlist_->blocks.push_back(b);
				PlaceBin pb;
				pb.x = px;
				pb.y = py;
				pb.pseudo_block_id = pseudo_block_id;
				bins.push_back(pb);
			}
		}

		double pxcenter = (netlist_->pxmax + netlist_->pxmin) * 0.5;
		double pycenter = (netlist_->pymax + netlist_->pymin) * 0.5;
		// Sort the bins in increasing distance away from the center of the placement area
		auto bin_comp = [&] (const PlaceBin& lhs, const PlaceBin& rhs) -> bool {
			double lhs_dist = std::abs(lhs.x - pxcenter) + std::abs(lhs.y - pycenter);
			double rhs_dist = std::abs(rhs.x - pxcenter) + std::abs(rhs.y - pycenter);
			return lhs_dist < rhs_dist;
		};
		std::sort(bins.begin(), bins.end(), bin_comp);
		//msg::info("   Created %d new pseudo blocks.", partition * partition);
	}

	void init_bins(int partition, double bin_x, double bin_y, PlaceBins& bins)
	{
		disconnect_old_pseudo_blocks();
		create_new_bins(partition, bin_x, bin_y, bins);
	}

	void get_movable_blocks(std::vector<int>& movable_blocks)
	{
		for(const Block& b : netlist_->blocks)
		{
			if(b.placed())
			{
				pl_assert(b.valid());
				pl_assert(!b.fixed());
				pl_assert(!b.pseudo());
				movable_blocks.push_back(b.id());
			}
		}
	}

	void alloc_bins(PlaceBins& bins)
	{
		std::vector<int> movable_blocks;
		get_movable_blocks(movable_blocks);
		while(!movable_blocks.empty())
		{
			for(PlaceBin& pb : bins)
			{
				const Block& pseudo_block = netlist_->blocks.at(pb.pseudo_block_id);
				pl_assert(pseudo_block.pseudo());
				pl_assert(pseudo_block.id() == pb.pseudo_block_id);
				auto comp_func = [&] (int lhs, int rhs) -> bool {
					const Block& lhs_b = netlist_->blocks.at(lhs);
					const Block& rhs_b = netlist_->blocks.at(rhs);

					double lhs_dist = std::abs(pb.x - lhs_b.x()) +
						std::abs(pb.y - lhs_b.y());
					double rhs_dist = std::abs(pb.x - rhs_b.x()) +
						std::abs(pb.y - rhs_b.y());
					return lhs_dist > rhs_dist;
				};
				std::sort(movable_blocks.begin(), movable_blocks.end(), comp_func);
				int selected_block = movable_blocks.back();
				movable_blocks.pop_back();
				pb.blocks.push_back(selected_block);
				if(!movable_blocks.empty())
				{
					selected_block = movable_blocks.back();
					movable_blocks.pop_back();
					pb.blocks.push_back(selected_block);
				}
				if(movable_blocks.empty())
				{
					break;
				}
			}
		}

		// Hack to ensure the [] operator is accessible in the debugger
		pl_assert(bins.at(0).blocks.size() >= 0);
	}

	void connect_pseudo_blocks(const PlaceBins& bins)
	{
		for(const PlaceBin& pb : bins)
		{
			Block& pseudo_block = netlist_->blocks.at(pb.pseudo_block_id);
			pl_assert(pseudo_block.id() == pb.pseudo_block_id);
			pl_assert(pseudo_block.pseudo());
			for(int block_id : pb.blocks)
			{
				Block& b = netlist_->blocks.at(block_id);
				pl_assert(b.id() == block_id);
				pl_assert(b.placed());
				pseudo_block.connect_to_block(block_id);
				b.connect_to_block(pseudo_block.id());
			}
		}
	}

	void cleanup()
	{
		block_id_map_.clear();
		place_blocks_.clear();
		netlist_ = nullptr;
		place_bins_.clear();
	}
}

namespace placer
{
	void solve(Netlist& netlist, int iteration)
	{
		pl_assert(netlist_ == nullptr);
		netlist_ = &netlist;
		init_solve_global_data();
		if(iteration == 0)
		{
			msg::info("   There are totally %zu blocks out of which %zu are movable.",
			netlist.blocks.size() - 1,
			place_blocks_.size()
			);
		}

		Placement1D px, py;
		px.resize(place_blocks_.size());
		py.resize(place_blocks_.size());
		solve_x_coord(px);
		solve_y_coord(py);

		for(size_t i = 0; i < px.size(); ++i)
		{
			Block *block = place_blocks_.at(i);
			block->place(px.at(i), py.at(i));
#if 0
			msg::debug("Block %d placed at (%.2f, %.2f)", block->id(),
				px.at(i),
				py.at(i)
			);
#endif
		}

		cleanup();
	}

	bool spread(Netlist& netlist, int iteration)
	{
		pl_assert(netlist_ == nullptr);
		netlist_ = &netlist;
		int partition = std::pow(2, netlist_->partition);
		// Don't partition beyond the die's cell size
		if(partition > 10)
		{
			partition = 10;
		}
		double bin_x = (netlist.xmax - netlist.xmin) / partition;
		double bin_y = (netlist.ymax - netlist.ymin) / partition;
		msg::info("   Spread is paritioning by %d. Bin size is %.4f by %.4f. Edge weight fact is %.4f.", 
			partition,
			bin_x,
			bin_y,
			netlist_->pedge_fact);

		PlaceBins bins;
		init_bins(partition, bin_x, bin_y, bins);
		alloc_bins(bins);
		connect_pseudo_blocks(bins);

		if(iteration < 3)
		{
			netlist_->partition += 1;

		}
		else
		{
			netlist_->pedge_fact *= 1.11;
		}
		cleanup();
		return false;
	}
}