#include "utils.h"
#include "graphics.h"
#include "place_algorithm.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <execinfo.h>
#include <unistd.h>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <limits>
#include <vector>
#include <cmath>
#include <random>

namespace msg
{
	void info(const char *fmt, ...)
	{
		va_list args;
    	va_start(args, fmt);
    	printf("Info: ");
    	vprintf(fmt, args);
    	printf("\n");
    	va_end(args);
	}

	void warn(const char *fmt, ...)
	{
		va_list args;
    	va_start(args, fmt);
    	printf("Warning: ");
    	vprintf(fmt, args);
    	printf("\n");
    	va_end(args);
	}

	void error(const char *fmt, ...)
	{
		va_list args;
    	va_start(args, fmt);
    	printf("Error: ");
    	vprintf(fmt, args);
    	printf("\n");
    	va_end(args);
	}

	void debug(const char *fmt, ...)
	{
		va_list args;
    	va_start(args, fmt);
    	printf("Debug: ");
    	vprintf(fmt, args);
    	printf("\n");
    	va_end(args);
	}
}

namespace
{
    const int BLOCK_X = 70;
    const int BLOCK_Y = 70;
    const int BLOCK_RADIUS = 35;
    const int GRID_DIM = 10;
    const double EPSILON = 0.005;
    const placer::Netlist *netlist_ = nullptr;
    bool draw_graphics_ = true;
    bool draw_wires_ = true;
    bool draw_bins_ = false;
    bool buttons_added_ = false;
    int num_iterations_ = 100;
    double overlap_limit_ = 0.15;
    int draw_interval_ = 10;
    bool randomize_fixed_placement_ = false;
    std::vector<std::vector<int>> occupancy_grid_;
    double pedge_fact_ = 2.0;

    placer::Block read_net(std::string net, size_t line)
    {
        std::istringstream is(net);

        int block_id;
        if(!(is >> block_id))
        {
            msg::error("Failed to parse block id in circuit file line %zu", line);
            pl_die;
        }
        
        placer::Block b(block_id);
        int net_id;
        while((is >> net_id) && (net_id != -1))
        {
            b.add_net(net_id);
        }
        if(net_id != -1)
        {
            msg::warn("Improperly terminated block on line %zu. Missing -1", line);
        }
        return b;
    }

    void read_fixed_block(std::string block, placer::Netlist& netlist, size_t line)
    {
        std::istringstream is(block);

        int block_id, x, y;
        if(!(is >> block_id))
        {
            msg::error("Failed to parse block id in circuit file line %zu", line);
            pl_die;
        }
        if(!(is >> x))
        {
            msg::error("Failed to parse block x coordinate in circuit file line %zu", line);
            pl_die;
        }
        if(!(is >> y))
        {
            msg::error("Failed to parse block y coordinate circuit file line %zu", line);
            pl_die;
        }

        netlist.blocks.at(block_id).fix(x, y);
    }

    int count_movable_blocks(const placer::Netlist& netlist)
    {
        int count = 0;
        for(const placer::Block& b : netlist.blocks)
        {
            if(b.placed())
            {
                ++count;
            }
            else
            {
                pl_assert(!b.valid() || b.fixed());
            }
        }
        pl_assert(count > 0);
        return count;
    }

    void summarize_blocks(const std::map<int, placer::Block>& blocks, placer::Netlist& netlist, int max_id)
    {
        netlist.blocks.resize(max_id + 1);
        std::map<int, placer::Block>::const_iterator it;
        int net_max = -1;
        for(it = blocks.begin(); it != blocks.end(); ++it)
        {
            placer::Block b = it->second;
            netlist.blocks.at(b.id()) = b;
            for(size_t inet = 0; inet < b.nets().size(); ++inet)
            {
                if(b.nets().at(inet) > net_max)
                {
                    net_max = b.nets().at(inet);
                }
            }
        }

        netlist.nets.resize(net_max + 1);
        pl_assert(!netlist.blocks[0].valid());
        for(size_t iblock = 1; iblock < netlist.blocks.size(); ++iblock)
        {
            pl_assert(netlist.blocks[iblock].valid());
            const placer::Block::Nets& nets = netlist.blocks[iblock].nets();
            for(size_t inet = 0; inet < nets.size(); ++inet)
            {
                int net_id = nets[inet];
                netlist.nets.at(net_id).blocks.push_back(iblock);
            }
        }

        for(size_t inet = 0; inet < netlist.nets.size(); ++inet)
        {
            for(size_t iblock = 0; iblock < netlist.nets[inet].blocks.size(); ++iblock)
            {
                int iblock_id = netlist.nets[inet].blocks[iblock];
                for(size_t jblock = iblock + 1; jblock < netlist.nets[inet].blocks.size(); ++jblock)
                {
                    int jblock_id = netlist.nets[inet].blocks[jblock];
                    if(iblock == jblock)
                    {
                        continue;
                    }
                    netlist.blocks[iblock_id].connect_to_block(netlist.blocks[jblock_id].id());
                    netlist.blocks[jblock_id].connect_to_block(netlist.blocks[iblock_id].id());
                }
            }
        }

        for(size_t iblock = 1; iblock < netlist.blocks.size(); ++iblock)
        {
            netlist.blocks[iblock].finalize();
        }
    }

    void update_cur_placement_bounds(placer::Netlist& netlist)
    {
        double xmin = 0;
        double ymin = 0;
        double xmax = netlist.blocks.at(1).x();
        double ymax = netlist.blocks.at(1).y();

        for(const placer::Block& b : netlist.blocks)
        {
            if(b.placed())
            {
                netlist.pxmin = b.x();
                netlist.pymin = b.y();
                netlist.pxmax = b.x();
                netlist.pymax = b.y();
            }
        }
        for(const placer::Block& b : netlist.blocks)
        {
            if(!b.valid())
            {
                continue;
            }
            pl_assert(b.fixed() || b.placed());
            if(b.x() < xmin)
            {
                xmin = b.x();
            }
            if(b.x() > xmax)
            {
                xmax = b.x();
            }
            if(b.y() < ymin)
            {
                ymin = b.y();
            }
            if(b.y() > ymax)
            {
                ymax = b.y();
            }
            if(b.placed())
            {
                if(b.x() < netlist.pxmin)
                {
                    netlist.pxmin = b.x();
                }
                if(b.x() > netlist.pxmax)
                {
                    netlist.pxmax = b.x();
                }
                if(b.y() < netlist.pymin)
                {
                    netlist.pymin = b.y();
                }
                if(b.y() > netlist.pymax)
                {
                    netlist.pymax = b.y();
                }
            }
        }
        netlist.xmin = xmin;
        netlist.ymin = ymin;
        netlist.xmax = xmax;
        netlist.ymax = ymax;
    }

    void init_graphics(const placer::Netlist& netlist)
    {
        init_graphics("Placement", WHITE);
        set_visible_world(t_bound_box(netlist.xmin * BLOCK_X, 
            netlist.ymin * BLOCK_Y, 
            netlist.xmax * BLOCK_X, 
            netlist.ymax * BLOCK_Y)
        );
    }

    void get_block_xy(const placer::Block& b, double& x, double& y)
    {
        x = b.x() * BLOCK_X;
        y = b.y() * BLOCK_Y;
    }

    void get_block_occupancy_grid_xy(const placer::Block& b, int& x, int& y)
    {
        x = b.x() / GRID_DIM;
        y = b.y() / GRID_DIM;
        pl_assert(x >= 0 && x < GRID_DIM);
        pl_assert(y >= 0 && y < GRID_DIM);
    }

    void draw_block_connections(const placer::Block& b)
    {
        double x, y;
        get_block_xy(b, x, y);
        const placer::Block::Blocks& connected_blocks = b.connected_blocks();
        for(int cblock_id : connected_blocks)
        {
            double cx, cy;
            const placer::Block& cb = netlist_->blocks.at(cblock_id);
            if(cb.pseudo())
            {
                continue;
            }
            get_block_xy(cb, cx, cy);
            drawline(x, y, cx, cy);
        }
    }

    void clear_block_area(const placer::Block& b)
    {
        double x, y;
        get_block_xy(b, x, y);
        setcolor(WHITE);
        fillarc(t_point(x, y), BLOCK_RADIUS, 0, 360);
        setcolor(BLACK);
    }

    void draw_block(const placer::Block& b)
    {
        double x, y;
        float max = std::numeric_limits<float>::max();
        get_block_xy(b, x, y);

        if(b.placed())
        {
            int gx, gy;
            get_block_occupancy_grid_xy(b, gx, gy);
            if(occupancy_grid_.at(gx).at(gy) > 2)
            {
                setcolor(RED);
            }
        }
        drawarc(x, y, BLOCK_RADIUS, 0, 360);
        std::ostringstream oss;
        oss << b.id();
        drawtext(x, y, oss.str(), max, max);
        setcolor(BLACK);
    }

    void draw_bins()
    {
        setcolor(BLUE);
        for(int i = 0; i < GRID_DIM; ++i)
        {
            for(int j = 0; j < GRID_DIM; ++j)
            {
                drawrect(
                    i * BLOCK_X * GRID_DIM, 
                    j * BLOCK_Y * GRID_DIM, 
                    (i + 1) * BLOCK_X * GRID_DIM, 
                    (j + 1) * BLOCK_Y * GRID_DIM);
            }
        }
        setcolor(GREEN);
        for(const placer::Block& b : netlist_->blocks)
        {
            if(!b.pseudo())
            {
                continue;
            }
            double x, y;
            get_block_xy(b, x, y);
            drawarc(x, y, BLOCK_RADIUS, 0, 360);
        }
        setcolor(BLACK);
    }

    void draw_blocks()
    {
        clearscreen();
        if(draw_wires_)
        {
            for(const placer::Block& b : netlist_->blocks)
            {
                if(!b.valid() || b.pseudo())
                {
                    continue;
                }
                draw_block_connections(b);
            }
            for(const placer::Block& b : netlist_->blocks)
            {
                if(!b.valid() || b.pseudo())
                {
                    continue;
                }
                clear_block_area(b);
            }
        }

        for(const placer::Block& b : netlist_->blocks)
        {
            if(!b.valid() || b.pseudo())
            {
                continue;
            }
            draw_block(b);
        }
        if(draw_bins_)
        {
            draw_bins();
        }
        flushinput();
    }

    void toggle_wires(void (*drawscreen_fp) (void))
    {
        draw_wires_ = !draw_wires_;
        drawscreen_fp();
    }

    void toggle_bins(void (*drawscreen_fp) (void))
    {
        draw_bins_ = !draw_bins_;
        drawscreen_fp();
    }

    void draw(const placer::Netlist& netlist, double overlap, int iteration)
    {
        if(!draw_graphics_)
        {
            return;
        }
        init_graphics(netlist);
        if(!buttons_added_)
        {
            create_button("Window", "Toggle Wires", toggle_wires);
            create_button("Toggle Wires", "Toggle Bins", toggle_bins);
            buttons_added_ = true;
        }

        pl_assert(netlist_ == nullptr);
        netlist_ = &netlist;
        draw_blocks();
        std::ostringstream oss;
        oss << "Iteration " << iteration << ". Current overlap is " << overlap;
        update_message(oss.str().c_str());
        event_loop(NULL, NULL, NULL, draw_blocks);
        netlist_ = nullptr;
    }

    double report_hpwl(const placer::Netlist& netlist)
    {
        double hpwl = 0.0;
        for(const placer::Net& net : netlist.nets)
        {
            double net_hpwl = 0.0;
            if(net.blocks.empty())
            {
                continue;
            }
            double xmin, ymin, xmax, ymax;
            const placer::Block& first_block = netlist.blocks.at(net.blocks.at(0));
            xmin = first_block.x();
            ymin = first_block.y();
            xmax = xmin;
            ymax = ymin;
            for(int block_id : net.blocks)
            {
                const placer::Block& b = netlist.blocks[block_id];
                pl_assert(b.valid());
                pl_assert(!b.pseudo());
                pl_assert(b.fixed() || b.placed());
                if(b.x() < xmin)
                {
                    xmin = b.x();
                }
                if(b.y() < ymin)
                {
                    ymin = b.y();
                }
                if(b.x() > xmax)
                {
                    xmax = b.x();
                }
                if(b.y() > ymax)
                {
                    ymax = b.y();
                }
            }
            net_hpwl += (xmax - xmin) + (ymax - ymin);
            hpwl += net_hpwl;
        }
        msg::info("   HPWL is %.2f.", hpwl);
        return hpwl;
    }

    void init_occupancy_grid()
    {
        if(occupancy_grid_.empty())
        {
            occupancy_grid_.resize(GRID_DIM);
        }
        for(int i = 0; i < GRID_DIM; ++i)
        {
            if(occupancy_grid_[i].empty())
            {
                occupancy_grid_[i].resize(GRID_DIM);
            }
            for(int j = 0; j < GRID_DIM; ++j)
            {
                occupancy_grid_[i][j] = 0;
            }
        }
    }

    void update_occupancy_grid(const placer::Netlist& netlist)
    {
        init_occupancy_grid();
        for(const placer::Block& b : netlist.blocks)
        {
            // Discard if not a movable block
            if(!b.placed())
            {
                continue;
            }

            int x, y;
            get_block_occupancy_grid_xy(b, x, y);
            occupancy_grid_.at(x).at(y)++;
            //msg::debug("  Block %d placed in (%d, %d)", b.id(), x, y);
        }
    }

    bool bin_densities_legal(const placer::Netlist& netlist, double& overlap)
    {
        overlap = 0.0;
        for(int i = 0; i < GRID_DIM; ++i)
        {
            for(int j = 0; j < GRID_DIM; ++j)
            {
                if(occupancy_grid_.at(i).at(j) > 2)
                {
                    overlap += (occupancy_grid_[i][j] - 2);
                }
            }
        }
        overlap /= count_movable_blocks(netlist);
        msg::info("   Current overlap is %.4f.", overlap);
        if(overlap <= overlap_limit_)
        {
            return true;
        }

        return false;
    }

    void randomize_fixed_placement(placer::Netlist& netlist)
    {   std::vector<int> fixed_block_ids;
        for(const placer::Block& b : netlist.blocks)
        {
            if(b.fixed() && !b.pseudo())
            {
                fixed_block_ids.push_back(b.id());
            }
        }
        std::default_random_engine generator;
        std::uniform_int_distribution<size_t> distribution(0, fixed_block_ids.size() - 1);
        int swaps = 0;
        while(swaps != 100)
        {
            size_t i = distribution(generator);
            size_t j = distribution(generator);
            if(i == j)
            {
                continue;
            }
            double ix, iy;
            double jx, jy;
            placer::Block& ib = netlist.blocks.at(fixed_block_ids[i]);
            placer::Block& jb = netlist.blocks.at(fixed_block_ids[j]);
            ix = ib.x();
            iy = ib.y();
            jx = jb.x();
            jy = jb.y();
            ib.refix(jx, jy);
            jb.refix(ix, iy);
            swaps++;
        }
    }

    void unplace_netlist(placer::Netlist& netlist)
    {
        for(placer::Block& b : netlist.blocks)
        {
            if(b.placed())
            {
                b.unplace();
            }
        }
    }
}

namespace utils
{
    void cmd_help()
    {
        msg::info("Usage: ./Placer <circuit_filename> {other optional arguments}");
        msg::info("Example Usage: ./Placer cct1.txt -no_graphics -iterations=120");
        msg::info("Optional arguments:-");
        msg::info("-no_graphics                          Disables graphics. Useful for experimentation.");
        msg::info("-overlap_limit=<float>                Sets the overlap limit at which the placer will");
        msg::info("                                      declare its current placement a success. 0.15");
        msg::info("                                      by default");
        msg::info("-iterations=<int>                     Max number of iterations that placer will run.");
        msg::info("                                      100 by default.");
        msg::info("-draw_interval=<int>                  The frequency at which the placer will draw the");
        msg::info("                                      graphics to display current progress. 10 by");
        msg::info("                                      default. Set to 1 if you want to display progress");
        msg::info("                                      every iteration.");
        msg::info("-randomize_fixed_placement            Randomize the placement of fixed blocks to examine");
        msg::info("                                      the impact of fixed block placement on HPWL.");
        msg::info("                                      Spreading is disabled in this mode.");
        msg::info("-pedge_fact=<float>                   Weight of edges to/from pseudo blocks.");
    }

    bool process_cmd_args(int ac, char *av[])
    {
        bool errors = false;
        for(int iarg = 0; iarg < ac; ++iarg)
        {
            const std::string arg = av[iarg];
            if(arg == "-no_graphics")
            {
                draw_graphics_ = false;
            }
            else if(sscanf(arg.c_str(), "-iterations=%d", &num_iterations_) == 1)
            {
                if(num_iterations_ <= 0)
                {
                    msg::error("Iteration count %d is invalid. Must be a positive integer.", num_iterations_);
                    errors = true;
                }
            }
            else if(sscanf(arg.c_str(), "-overlap_limit=%lf", &overlap_limit_) == 1)
            {
                if(overlap_limit_ < 0)
                {
                    msg::error("Overlap limit %f is invalid. Must be a positive number.", overlap_limit_);
                    errors = true;
                }
            }
            else if(sscanf(arg.c_str(), "-pedge_fact=%lf", &pedge_fact_) == 1)
            {
                if(pedge_fact_ <= 0)
                {
                    msg::error("pedge_fact %f is invalid. Must be a positive number.", pedge_fact_);
                    errors = true;
                }
            }
            else if(sscanf(arg.c_str(), "-draw_interval=%d", &draw_interval_) == 1)
            {
                if(draw_interval_ <= 0)
                {
                    msg::error("Draw interval %d is invalid. Must be a positive integer.", draw_interval_);
                    errors = true;
                }
            }
            else if(arg == "-help" || arg == "-h")
            {
                cmd_help();
                exit(EXIT_SUCCESS);
            }
            else if(arg == "-randomize_fixed_placement")
            {
                randomize_fixed_placement_ = true;
            }
            else
            {
                msg::error("Unrecognized argument '%s'", arg.c_str());
                errors = true;
            }

        }

        if(errors)
        {
            cmd_help();
            return false;
        }

        if(!draw_graphics_)
        {
            msg::info("Graphics has been disabled.");
        }
        msg::info("Placer will run upto %d iteration(s).", num_iterations_);
        msg::info("Legal overlap limit has been set to %f", overlap_limit_);
        if(draw_graphics_)
        {
            msg::info("Graphics will be drawn every %d iteration(s)", draw_interval_);
        }
        if(randomize_fixed_placement_)
        {
            msg::info("Fixed block placement will be randomized each iteration. Spreading has been disabled.");
        }
        msg::info("Pseudo Block edge weight set to %f", pedge_fact_);
        return true;
    }

    bool read_circuit(const char *filename, placer::Netlist& netlist)
    {
        std::ifstream fp(filename);
        if(fp.fail() || fp.bad())
        {
            msg::error("Failed to open circuit file '%s'.", filename);
            cmd_help();
            return false;
        }
        msg::info("Reading circuit file '%s'.", filename);
        std::string buf;
        bool section_end = false;
        std::map<int, placer::Block> blocks;
        size_t line = 0;
        int max_block_id = -1;
        while(!fp.eof() && !fp.fail() && !fp.bad())
        {
            std::getline(fp, buf);
            ++line;
            if(buf.empty())
            {
                continue;
            }
            if(buf == "-1")
            {
                if(!section_end)
                {
                    summarize_blocks(blocks, netlist, max_block_id);
                }
                section_end = true;
                continue;
            }
            if(section_end)
            {
                read_fixed_block(buf, netlist, line);
            }
            else
            {
                placer::Block b = read_net(buf, line);
                if(blocks.find(b.id()) != blocks.end())
                {
                    msg::error("Block %d defined multiple times in circuit file.", b.id());
                    pl_die;
                }
                if(b.id() > max_block_id)
                {
                    max_block_id = b.id();
                }
                blocks.insert(std::pair<int, placer::Block> (b.id(), b) );
            }

        }
        netlist.pedge_fact = pedge_fact_;
        return true;
    }

    bool run_flow(placer::Netlist& netlist)
    {
        bool result = false;
        double initial_hpwl = 0.00000001, avg_hpwl = 0, hpwl = 0;
        double min_hpwl = 0;
        double max_hpwl = 0;
        int i;
        for(i = 0; i < num_iterations_; ++i)
        {
            msg::info("Placement iteration %d.", i);
            placer::solve(netlist, i);
            hpwl = report_hpwl(netlist);
            if(i == 0)
            {
                initial_hpwl = hpwl;
                min_hpwl = hpwl;
            }
            if(hpwl < min_hpwl)
            {
                min_hpwl = hpwl;
            }
            if(hpwl > max_hpwl)
            {
                max_hpwl = hpwl;
            }
            avg_hpwl += hpwl;
            update_cur_placement_bounds(netlist);
            update_occupancy_grid(netlist);
            double overlap;
            result = bin_densities_legal(netlist, overlap);
            
            if(i % draw_interval_ == 0 || result)
            {
                draw(netlist, overlap, i);
            }

            if(result)
            {
                // Increment i so that the avg HPWL is calculated
                // correctly
                i++;
                break;
            }

            // In the randomize fixed placement flow we want to see the impact
            // of randomnly changing the placement of the fixed blocks on the
            // HPWL. We don't want to do any spreading.
            if(randomize_fixed_placement_)
            {
                randomize_fixed_placement(netlist);
            }
            else
            {
                placer::spread(netlist, i);
            }
            unplace_netlist(netlist);
        }

        avg_hpwl /= i;
        if(randomize_fixed_placement_)
        {
            msg::info("Min HPWL: %f, Max %f, Avg %f",
                min_hpwl,
                max_hpwl,
                avg_hpwl);
        }
        else
        {
            msg::info("Initial HPWL %f, Avg %f, Final %f, Change %.2f%%",
            initial_hpwl,
            avg_hpwl,
            hpwl,
            ((hpwl / initial_hpwl) - 1) * 100);
        }
        return result;
    }

	void die_at(const char *filename, int line)
	{
		const int TRACE_LIMIT = 20;
		void *array[TRACE_LIMIT];
		msg::error("Fatal exit at %s:%d", filename, line);
  		size_t size;

  		// get void*'s for all entries on the stack
  		size = backtrace(array, TRACE_LIMIT);
        msg::error("Begin Stack Trace:-");
  		backtrace_symbols_fd(array, size, STDOUT_FILENO);
        msg::error("End Trace");
		exit(EXIT_FAILURE);
	}
}