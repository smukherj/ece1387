#include "partition.h"
#include "utils.h"
#include "block.h"
#include "netlist.h"
#include "easygl_constants.h"
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <chrono>
#include <thread>
#include <mutex>

namespace
{
	typedef std::unordered_map<int, std::unordered_set<int>> EdgeMap;

	int best_crossing_count_ = 0;
	std::mutex cc_mutex_;
	const size_t THREAD_NODE_LIMIT = 10000000;

	struct BBStackElem
	{
		int block_index;
		int cc_diff;
		bool tried_left;
		bool tried_right;
		int num_nets_killed;

		BBStackElem() : 
		block_index(-1), 
		cc_diff(0),
		tried_left(false),
		tried_right(false),
		num_nets_killed(0)
		{}
	};

	struct WorkerData
	{
		Netlist *netlist;
		std::vector<int> blocks;
		std::vector<Partition> partitions;
		std::vector<int> id_to_idx;
		std::vector<BBStackElem> block_stack;
		std::vector<bool> dead_nets;
		std::vector<int> nets_killed;
		int cross_count;
		int left_count;
		int right_count;
		int bin_limit;
		size_t nodes_visited;
		size_t thread_nodes_visited;
		size_t final_stack_size;

		WorkerData() :
		netlist(nullptr),
		cross_count(0),
		left_count(0),
		right_count(0),
		bin_limit(0),
		nodes_visited(0),
		thread_nodes_visited(0),
		final_stack_size(0)
		{
			blocks.reserve(60);
			partitions.reserve(60);
			block_stack.reserve(60);
		}

		bool place_left(int block_index, int& cc_diff, int& num_nets_killed);
		bool place_right(int block_index, int& cc_diff, int& num_nets_killed);
		void unplace(int block_index, int cc_diff, int& num_nets_killed);
		void check_bin_usage();
		int eval_cc_diff(int block_index, int& num_nets_killed);
		int calculate_cc_count();
		void assign_initial_partition();
		void unplace_all();
	};

	bool WorkerData::place_left(int block_index, int& cc_diff, int& num_nets_killed)
	{
		if(left_count >= bin_limit)
		{
			return false;
		}
#ifdef DEBUG
		pt_assert(partitions.at(block_index) == Partition::OPEN);
#endif
		partitions[block_index] = Partition::LEFT;
		cc_diff = eval_cc_diff(block_index, num_nets_killed);
		cross_count += cc_diff;
		left_count += 1;
		return true;
	}

	bool WorkerData::place_right(int block_index, int& cc_diff, int& num_nets_killed)
	{
		if(right_count >= bin_limit)
		{
			return false;
		}
#ifdef DEBUG
		pt_assert(partitions.at(block_index) == Partition::OPEN);
#endif
		partitions[block_index] = Partition::RIGHT;
		cc_diff = eval_cc_diff(block_index, num_nets_killed);
		cross_count += cc_diff;
		right_count += 1;
		return true;
	}

	void WorkerData::unplace(int block_index, int cc_diff, int& num_nets_killed)
	{
		if(partitions[block_index] != Partition::OPEN)
		{
			if(partitions[block_index] == Partition::LEFT)
			{
				left_count--;
			}
			else
			{
				right_count--;
			}
			partitions[block_index] = Partition::OPEN;
			cross_count -= cc_diff;
			while(num_nets_killed)
			{
				int net_id = nets_killed.back();
				dead_nets[net_id] = false;
				nets_killed.pop_back();
				--num_nets_killed;
			}
		}
	}

	void WorkerData::check_bin_usage()
	{
		int left_count_test = 0;
		int right_count_test = 0;
		for(Partition p : partitions)
		{
			if(p == Partition::LEFT)
			{
				++left_count_test;
			}
			else if(p == Partition::RIGHT)
			{
				++right_count_test;
			}
		}

		if(left_count_test != left_count || right_count_test != right_count)
		{
			msg::debug("Bin usage check failed:-");
			msg::debug("left_count: %d, right_count: %d", left_count, right_count);
			msg::debug("Evaluated: %d, %d", left_count_test, right_count_test);
			pt_die;
		}
	}

	/* Update crossing count
	 */
	int WorkerData::eval_cc_diff(int block_index, int& num_nets_killed)
	{
		int result = 0;
		int block_id = blocks[block_index];

		for(int inet : netlist->block(block_id).nets())
		{
			if(dead_nets[inet])
			{
				continue;
			}
			const auto& net = netlist->net(inet);
			bool found_left = false;
			bool found_right = false;
			bool all_placed = true;
			for(int connected_block_id : net)
			{
				int connected_block_index = id_to_idx[connected_block_id];
#ifdef DEBUG
				pt_assert(blocks.at(connected_block_index) == connected_block_id);
#endif
				Partition p = partitions[connected_block_index];

				if(p == Partition::LEFT)
				{
					found_left = true;
				}
				else if(p == Partition::RIGHT)
				{
					found_right = true;
				}
				else
				{
					all_placed = false;
				}

				if(found_left && found_right)
				{
					break;
				}
			}
			if(found_left && found_right)
			{
				dead_nets[inet] = true;
				nets_killed.push_back(inet);
				num_nets_killed++;
				result += 1;
			}
			else if(all_placed)
			{
				dead_nets[inet] = true;
				nets_killed.push_back(inet);
				num_nets_killed++;
			}
		}
		return result;
	}

	int WorkerData::calculate_cc_count()
	{
		int result = 0;
		for(const auto& net : netlist->nets())
		{
			bool found_left = false;
			bool found_right = false;
			for(int connected_block_id : net)
			{
				int connected_block_index = id_to_idx[connected_block_id];
#ifdef DEBUG
				pt_assert(blocks.at(connected_block_index) == connected_block_id);
#endif
				Partition p = partitions[connected_block_index];

				if(p == Partition::LEFT)
				{
					found_left = true;
				}
				else if(p == Partition::RIGHT)
				{
					found_right = true;
				}

				if(found_left && found_right)
				{
					break;
				}
			}
			if(found_left && found_right)
			{
				result += 1;
			}
		}
		return result;
	}

	void WorkerData::assign_initial_partition()
	{
		auto blocks = netlist->blocks();
		pt_assert(blocks.size() % 2 == 0);
		int idx = -1;

		std::sort(blocks.begin(), blocks.end(), [&] (const Block& lhs, const Block& rhs) -> bool {
			return netlist->block(lhs.id()).nets().size() > netlist->block(rhs.id()).nets().size();
		});
		this->id_to_idx.resize(blocks.size() + 1, -1);
		for(const Block& block : blocks)
		{
			this->id_to_idx.at(block.id()) = static_cast<int>(this->blocks.size());
			this->blocks.push_back(block.id());
			idx++;
			partitions.push_back(Partition::OPEN);
		}
		auto nets = netlist->nets();
		std::sort(nets.begin(), nets.end(), [&] (const std::vector<int>& lhs, const std::vector<int>& rhs) -> bool {
			return lhs.size() > rhs.size();
		});

		int initial_left_count = 0;
		int bin_limit = (int)blocks.size() * 0.5;
		for(const auto& net : nets)
		{
			for(int block_id : net)
			{
				if(initial_left_count >= bin_limit)
				{
					break;
				}
				int block_index = id_to_idx.at(block_id);
				pt_assert(this->blocks.at(block_index) == block_id);
				if(partitions.at(block_index) == Partition::OPEN)
				{
					++initial_left_count;
					partitions.at(block_index) = Partition::LEFT;
				}
			}
			if(initial_left_count >= bin_limit)
			{
				break;
			}
		}
		int initial_right_count = 0;
		for(size_t i = 0; i < partitions.size(); ++i)
		{
			if(partitions[i] == Partition::OPEN)
			{
				partitions[i] = Partition::RIGHT;
				++initial_right_count;
			}
		}
		pt_assert(initial_right_count > 0);
		pt_assert(initial_left_count == initial_right_count);
		pt_assert((initial_left_count + initial_right_count) == (int)partitions.size());

		this->cross_count = calculate_cc_count();
		msg::info("Initial crossing count is %d", this->cross_count);
	}

	void WorkerData::unplace_all()
	{
		size_t size = this->partitions.size();
		for(size_t i = 0; i < size; ++i)
		{
			this->partitions[i] = Partition::OPEN;
		}
		for(size_t i = 0; i < dead_nets.size(); ++i)
		{
			dead_nets[i] = false;
		}
		nets_killed.clear();
		this->cross_count = 0;
	}

	void draw_state(const WorkerData& wd, bool prune)
	{
		if(!utils::draw_graphics())
		{
			return;
		}
		long long level = (long long)wd.block_stack.size();
		long long width = (1 << (long long)wd.blocks.size());
		long long iwidth = width >> 2;
		long long deviation = 0;
		float y = (wd.blocks.size() + 1) - level;
		for(size_t ielem = 1; ielem < wd.block_stack.size(); ++ielem, iwidth = iwidth >> 1)
		{
			Partition p = wd.partitions[ielem];
			pt_assert(p != Partition::OPEN);
			if(p == Partition::LEFT)
			{
				deviation -= iwidth;
			}
			else
			{
				deviation += iwidth;
			}
			//msg::debug("ielem: %zu, p: %c, iwidth: %ld, dev: %ld",
				//ielem,
				//p == Partition::LEFT ? 'l' : 'r',
				//iwidth,
				//deviation
				//);
		}
		float x = (width >> 1) + deviation;
		//msg::debug("Draw at (%.0f, %.0f). Stack depth %zu, width %ld, deviation %ld", x, y, level, width, deviation);
		utils::draw_node(x, y, prune ? RED : BLUE);
	}

	bool try_left(WorkerData& wd, BBStackElem& head, int last_block, WorkerData& best_wd)
	{
		bool continue_iteration = false;
		if(!head.tried_left)
		{
			head.tried_left = true;
			wd.nodes_visited++;
			if(wd.place_left(head.block_index, head.cc_diff, head.num_nets_killed))
			{
				if(wd.cross_count < best_crossing_count_)
				{
					draw_state(wd, false);
					if(head.block_index < last_block)
					{
#ifdef PARTITIONER_VERBOSE
						msg::debug("   %d to left", head.block_index);
#endif
						BBStackElem child;
						child.block_index = head.block_index + 1;
						wd.block_stack.push_back(child);
						continue_iteration = true;
					}
					else
					{
						best_wd = wd;
						cc_mutex_.lock();
						if(wd.cross_count < best_crossing_count_)
						{
							best_crossing_count_ = wd.cross_count;
						}
						cc_mutex_.unlock();
						msg::info("Best crossing count updated to %d", best_wd.cross_count);
						wd.unplace(head.block_index, head.cc_diff, head.num_nets_killed);
					}
				}
				else
				{
					draw_state(wd, true);
					wd.unplace(head.block_index, head.cc_diff, head.num_nets_killed);
				}
			}
		}
		return continue_iteration;
	}

	bool try_right(WorkerData& wd, BBStackElem& head, int last_block, WorkerData& best_wd)
	{
		bool continue_iteration = false;
		if(!head.tried_right)
		{
			head.tried_right = true;
			wd.nodes_visited++;
			if(wd.place_right(head.block_index, head.cc_diff, head.num_nets_killed))
			{
				if(wd.cross_count < best_crossing_count_)
				{
					draw_state(wd, false);
					if(head.block_index < last_block)
					{
#ifdef PARTITIONER_VERBOSE
						msg::debug("   %d to right", head.block_index);
#endif
						BBStackElem child;
						child.block_index = head.block_index + 1;
						wd.block_stack.push_back(child);
						continue_iteration = true;
					}
					else
					{
						best_wd = wd;
						cc_mutex_.lock();
						if(wd.cross_count < best_crossing_count_)
						{
							best_crossing_count_ = wd.cross_count;
						}
						cc_mutex_.unlock();
						msg::info("Best crossing count updated to %d", best_wd.cross_count);
						wd.unplace(head.block_index, head.cc_diff, head.num_nets_killed);
					}
				}
				else
				{
					draw_state(wd, true);
					wd.unplace(head.block_index, head.cc_diff, head.num_nets_killed);
				}
			}
		}
		return continue_iteration;
	}

	void branch_and_bound(WorkerData& wd, WorkerData& best_wd, int id = -1)
	{
		if(id != -1)
		{
			//msg::info("Parallel worker %d is alive", id);
			wd.thread_nodes_visited = 0;
		}
		pt_assert(wd.block_stack.size() > 0);
		size_t initial_stack_depth = wd.block_stack.size() - 1;
		pt_assert(wd.blocks.size() > 1);
		pt_assert(wd.blocks.size() == wd.partitions.size());
		pt_assert(wd.blocks.size() % 2 == 0);
		pt_assert(wd.id_to_idx.size() == (wd.blocks.size() + 1));
		
		int last_block = (int)wd.blocks.size() - 1;
		
		while(wd.block_stack.size() != initial_stack_depth)
		{
			wd.thread_nodes_visited++;
			BBStackElem& head = wd.block_stack.back();
			Partition hp = wd.partitions[head.block_index];
			size_t stack_size = wd.block_stack.size();

#ifdef PARTITIONER_VERBOSE
			msg::debug("At Block Idx: %d, id: %d, ss: %zu, bcc: %d cc: %d, ccd: %d, nets_killed: %zu, bin usage (%d, %d), tl: %s, tr: %s", 
				head.block_index,
				wd.blocks[head.block_index],
				stack_size,
				best_wd.cross_count,
				wd.cross_count,
				head.cc_diff,
				head.nets_killed.size(),
				wd.left_count,
				wd.right_count,
				head.tried_left ? "y" : "n",
				head.tried_right ? "y" : "n"
				);
#endif

#ifdef DEBUG
			wd.check_bin_usage();
#endif

			wd.unplace(head.block_index, head.cc_diff, head.num_nets_killed);
			
			// Since blocks with lot of connections are located
			// in the beginning, try to pack them in the same
			// partition. Later we will alternate.
			if(stack_size <  3 || (stack_size & 0x1) == 0)
			{
				if(try_left(wd, head, last_block, best_wd))
				{				
					continue;
				}
				if(try_right(wd, head, last_block, best_wd))
				{
					continue;
				}
			}
			else
			{
				if(try_right(wd, head, last_block, best_wd))
				{
					continue;
				}
				if(try_left(wd, head, last_block, best_wd))
				{
					continue;
				}
			}
			
			if(head.tried_left && head.tried_right)
			{
				if(hp != Partition::OPEN)
				{
#ifdef PARTITIONER_VERBOSE
					msg::debug("   %d unplace", head.block_index);
#endif
					wd.unplace(head.block_index, head.cc_diff, head.num_nets_killed);
				}
				
				wd.block_stack.pop_back();
				if(id != -1 && wd.thread_nodes_visited >= THREAD_NODE_LIMIT)
				{
					break;
				}
			}
		}

		if(id != -1)
		{
			//msg::info("Parallel worker %d is done.", id);
		}
	}

	void bb_parallel_launch_and_wait(
		std::vector<WorkerData>& work_objs,
		std::vector<WorkerData>& best_wd_objs
	)
	{
		pt_assert(work_objs.size() == best_wd_objs.size());
		std::vector<std::thread> thread_objs;
		int thread_id = -1;
		bool all_completed = false;

		int thread_count = 0;
		while(!all_completed)
		{
			all_completed = true;
			for(size_t i = 0; i < work_objs.size(); ++i)
			{
				if(work_objs[i].block_stack.size() > work_objs[i].final_stack_size)
				{
					thread_objs.push_back(
						std::thread(
							branch_and_bound,
							std::ref(work_objs[i]),
							std::ref(best_wd_objs[i]),
							++thread_id
							)
						);
					all_completed = false;
					++thread_count;
				}
			}

			for(auto& t : thread_objs)
			{
				t.join();
			}
			thread_objs.clear();
		}
		msg::info("Totally %d threads were created", thread_count);
	}

	void bb_parallel_summarize_results(
		std::vector<WorkerData>& work_objs,
		std::vector<WorkerData>& best_wd_objs,
		WorkerData& wd,
		WorkerData& best_wd
	)
	{
		for(WorkerData& iwd : work_objs)
		{
			wd.nodes_visited += iwd.nodes_visited;
		}
		for(WorkerData& iwd : best_wd_objs)
		{
			if(iwd.cross_count < best_wd.cross_count)
			{
				best_wd = iwd;
			}
		}
	}

	void bb_parallel(WorkerData& wd, WorkerData& best_wd)
	{
		pt_assert(wd.block_stack.size() == 1);
		std::vector<WorkerData> work_objs;
		std::vector<WorkerData> best_wd_objs;

		int last_block = (int)wd.blocks.size() - 1;
		size_t last_level = wd.block_stack.size();

		while(last_level < 20 && ((1 << last_level) < utils::parallel()))
		{
			++last_level;
		}
		++last_level;

		while(!wd.block_stack.empty())
		{
			BBStackElem& head = wd.block_stack.back();
			Partition hp = wd.partitions[head.block_index];
			size_t stack_size = wd.block_stack.size();

#ifdef DEBUG
			wd.check_bin_usage();
#endif
			wd.unplace(head.block_index, head.cc_diff, head.num_nets_killed);
			
			if(wd.block_stack.size() >= last_level)
			{
				wd.final_stack_size = wd.block_stack.size() - 1;
				work_objs.push_back(wd);
				best_wd_objs.push_back(best_wd);
				head.tried_left = true;
				head.tried_right = true;
			}
			else if(stack_size <  3 || (stack_size & 0x1) == 0)
			{
				if(try_left(wd, head, last_block, best_wd))
				{				
					continue;
				}
				if(try_right(wd, head, last_block, best_wd))
				{
					continue;
				}
			}
			else
			{
				if(try_right(wd, head, last_block, best_wd))
				{
					continue;
				}
				if(try_left(wd, head, last_block, best_wd))
				{
					continue;
				}
			}
			
			if(head.tried_left && head.tried_right)
			{
				if(hp != Partition::OPEN)
				{
#ifdef PARTITIONER_VERBOSE
					msg::debug("   %d unplace", head.block_index);
#endif
					wd.unplace(head.block_index, head.cc_diff, head.num_nets_killed);
				}
				
				wd.block_stack.pop_back();
			}
		}
		bb_parallel_launch_and_wait(work_objs, best_wd_objs);
		bb_parallel_summarize_results(work_objs, best_wd_objs, wd, best_wd);
	}

	void print_placement(const WorkerData& best_wd)
	{
		msg::info("Partition results:-");
		for(size_t i = 0; i < best_wd.blocks.size(); ++i)
		{
			pt_assert(best_wd.partitions[i] != Partition::OPEN);
			msg::info("   Block %d to %s",
				best_wd.blocks[i],
				best_wd.partitions[i] == Partition::LEFT ?
				"left" : "right"
			);
		}
	}
}

bool partition(Netlist& netlist)
{
	msg::info("Netlist has %zu blocks and %zu nets", netlist.blocks().size(), netlist.nets().size());
	WorkerData wd, best_wd;
	wd.netlist = &netlist;
	wd.dead_nets.resize(wd.netlist->nets().size(), false);
	wd.assign_initial_partition();
	best_wd = wd;
	pt_assert(wd.cross_count == wd.calculate_cc_count());
	if(utils::override_bound())
	{
		best_wd.cross_count = utils::overridden_bound();
	}
	wd.unplace_all();
	wd.bin_limit = (int)wd.blocks.size() * 0.5;
	int num_nets_killed;
	pt_assert(wd.place_left(0, wd.cross_count, num_nets_killed));
	pt_assert(num_nets_killed == 0);
	pt_assert(wd.cross_count == 0);
	BBStackElem head_elem;
	head_elem.block_index = 1;
	wd.block_stack.push_back(head_elem);
	msg::info("Bin limit is %d", wd.bin_limit);

	msg::info("Beginning partioning operations.");
	auto start = std::chrono::system_clock::now();
	best_crossing_count_ = best_wd.cross_count;
	if(utils::parallel() > 1)
	{
		bb_parallel(wd, best_wd);
	}
	else
	{
		branch_and_bound(wd, best_wd);
	}
	best_crossing_count_ = 0;
	auto end = std::chrono::system_clock::now();
	double elapsed_seconds = std::chrono::duration_cast<
  		std::chrono::duration<double> >(end - start).count();
  	msg::info("Partitioning operations ending. Elapsed time was %.1f s", elapsed_seconds);
	msg::info("Best crossing count was %d. Traversed %zu decision nodes.", best_wd.cross_count, wd.nodes_visited);

	if(utils::print_placement())
	{
		print_placement(best_wd);
	}
	return true;
}