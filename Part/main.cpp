#include "utils.h"
#include "netlist.h"
#include "partition.h"

int main (int argc, char *argv[])
{
    if(argc < 2)
    {
        msg::error("Must give atleast one command line argument pointing to circuit file.");
        utils::cmd_help();
        return -1;
    }
    bool result = true;
    msg::info("*******************************************************");
    msg::info("Running Partitioner");
    msg::info("Compiled on %s, %s", __DATE__, __TIME__);
    Netlist netlist;
    result = result && utils::process_cmd_args(argc - 2, argv + 2);
    result = result && utils::read_circuit(argv[1], netlist);
    utils::setup_graphics(netlist);
    result = result && partition(netlist);
    utils::cleanup_graphics();
    if(result)
    {
        msg::info("Partitioner was successful.");
        return 0;
    }
    else
    {
        msg::error("Partitioner was unsuccessful.");
        return -1;
    }
}

