#ifndef PARTITION_H
#define PARTITION_H

class Netlist;

bool partition(Netlist& netlist);

#endif /* PARTITION_H */