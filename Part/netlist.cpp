#include "netlist.h"
#include "utils.h"
#include "block.h"
#include <algorithm>

Netlist::Netlist()
{
}

Netlist::~Netlist()
{

}

Block& Netlist::block(int id)
{
#ifdef DEBUG
	return m_blocks.at(id - 1);
#else
	return m_blocks[id - 1];
#endif
}

int Netlist::connected(int lhs_id, int rhs_id)
{
#ifdef DEBUG
	int count = m_edges.at(lhs_id).at(rhs_id);
	pt_assert(count == m_edges.at(rhs_id).at(lhs_id));
	return count;
#else
	return m_edges[lhs_id][rhs_id];
#endif
}

const std::vector<int>& Netlist::connections(int id) const
{
	return m_edges.at(id);
}

void Netlist::resize_nets(size_t size)
{
	m_nets.resize(size);
}

void Netlist::add_block(int id)
{
	pt_assert(id == static_cast<int>(m_blocks.size() + 1));
	m_blocks.push_back(Block(id));
}

void Netlist::add_block_to_net(int block_id, int net_id)
{
	pt_assert(std::find(m_nets.at(net_id).begin(), m_nets.at(net_id).end(), block_id) ==
		m_nets.at(net_id).end());
	m_nets.at(net_id).push_back(block_id);
}

void Netlist::connect_blocks(int lhs_id, int rhs_id)
{
	pt_assert(lhs_id > 0 && rhs_id > 0);
	pt_assert(lhs_id <= static_cast<int>(m_blocks.size()));
	pt_assert(rhs_id <= static_cast<int>(m_blocks.size()));
	if(m_edges.find(lhs_id) == m_edges.end() ||
		std::find(m_edges[lhs_id].begin(), m_edges[lhs_id].end(), rhs_id)
		== m_edges[lhs_id].end())
	{
		m_edges[lhs_id].push_back(rhs_id);
		m_edges[rhs_id].push_back(lhs_id);
	}
}