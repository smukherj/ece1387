#ifndef UTILS_H
#define UTILS_H

class Block;
class Netlist;

namespace msg
{
	void info(const char *fmt, ...);
	void warn(const char *fmt, ...);
	void error(const char *fmt, ...);
	void debug(const char *fmt, ...);
}

namespace utils
{
	void cmd_help();
	bool process_cmd_args(int ac, char *av[]);
	bool read_circuit(const char *filename, Netlist& netlist);
	void setup_graphics(const Netlist& netlist);
	void draw_node(float x, float y, int color);
	void cleanup_graphics();

	bool draw_graphics();
	bool override_bound();
	int overridden_bound();
	int parallel();
	bool print_placement();

	void die_at(const char *filename, int line);
}

#define pt_die {\
	utils::die_at(__FILE__, __LINE__);\
}

#define pt_assert(x) { \
	if(!(x)) {\
		msg::error("*****Assertion Failure***** '%s'", #x);\
		utils::die_at(__FILE__, __LINE__);\
	}\
}

#endif /* UTILS_H */