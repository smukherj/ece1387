#include "utils.h"
#include "graphics.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <execinfo.h>
#include <unistd.h>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <algorithm>
#include <thread>
#include <mutex>

#include "block.h"
#include "netlist.h"

namespace msg
{
	void info(const char *fmt, ...)
	{
		va_list args;
    	va_start(args, fmt);
    	printf("Info: ");
    	vprintf(fmt, args);
    	printf("\n");
    	va_end(args);
	}

	void warn(const char *fmt, ...)
	{
		va_list args;
    	va_start(args, fmt);
    	printf("WARN: ");
    	vprintf(fmt, args);
    	printf("\n");
    	va_end(args);
	}

	void error(const char *fmt, ...)
	{
		va_list args;
    	va_start(args, fmt);
    	printf("Error: ");
    	vprintf(fmt, args);
    	printf("\n");
    	va_end(args);
	}

	void debug(const char *fmt, ...)
	{
		va_list args;
    	va_start(args, fmt);
    	printf("Debug: ");
    	vprintf(fmt, args);
    	printf("\n");
    	va_end(args);
	}
}

namespace
{
    bool draw_graphics_ = true;
    bool override_bound_ = false;
    int overridden_bound_ = -1;
    int parallel_ = 1;
    bool print_placement_ = false;
    float x_scale_ratio = 1.0;
    float y_scale_ratio = 1.0;
    std::mutex graphics_mutex_;

    void read_net(std::string net, size_t line, int& block_id, std::vector<int>& nets)
    {
        std::istringstream is(net);

        if(!(is >> block_id))
        {
            msg::error("Failed to parse block id in circuit file line %zu", line);
            pt_die;
        }
        
        int net_id;
        while((is >> net_id) && (net_id != -1))
        {
            nets.push_back(net_id);
        }
        if(net_id != -1)
        {
            msg::warn("Improperly terminated block on line %zu. Missing -1", line);
        }
    }
}

namespace utils
{
    void cmd_help()
    {
        msg::info("Usage: ./Part <circuit_filename> {other optional arguments}");
        msg::info("Example Usage: ./Placer cct1.txt -no_graphics -iterations=120");
        msg::info("Optional arguments:-");
        msg::info("-h|-help                              Display this help and exit.");
        msg::info("-no_graphics                          Disables graphics. Useful for experimentation.");
        msg::info("-override_bound=<int>                 Overrides the minimum cut bound");
        msg::info("-parallel                             Run in parallel mode. Automatically detect number");
        msg::info("                                      of threads supported by system.");
        msg::info("-print_placement                      Print the placement of each cell (Left or right)");
    }

    bool process_cmd_args(int ac, char *av[])
    {
        bool errors = false;
        bool parallel = false;
        for(int iarg = 0; iarg < ac; ++iarg)
        {
            const std::string arg = av[iarg];
            if(arg == "-no_graphics")
            {
                draw_graphics_ = false;
            }
            else if(sscanf(arg.c_str(), "-override_bound=%d", &overridden_bound_) == 1)
            {
                if(overridden_bound_ <= 0)
                {
                    msg::error("Overridden bound %d is invalid. Must be a positive integer.", overridden_bound_);
                    errors = true;
                }
                override_bound_ = true;
            }
            else if(sscanf(arg.c_str(), "-parallel=%d", &parallel_) == 1)
            {
                if(parallel)
                {
                    msg::error("Parallel argument specified multiple times!");
                    errors = true;
                }
                parallel = true;
                if(parallel_ < 2)
                {
                    msg::error("Specified %d threads. Must be greater than 1", parallel_);
                }
            }
            else if(arg == "-parallel")
            {
                parallel_ = std::thread::hardware_concurrency() * 8;
                if(parallel)
                {
                    msg::error("Parallel argument specified multiple times!");
                    errors = true;
                }
                parallel = true;
            }
            else if(arg == "-print_placement")
            {
                print_placement_ = true;
            }
            else if(arg == "-help" || arg == "-h")
            {
                cmd_help();
                exit(EXIT_SUCCESS);
            }
            else
            {
                msg::error("Unknown argument '%s'", arg.c_str());
                errors = true;
            }

        }

        if(errors)
        {
            cmd_help();
            return false;
        }

        if(parallel)
        {
            msg::info("Partitioner will run in PARALLEL mode with %d threads", parallel_);
        }
        else
        {
            msg::info("Partitioner will run in SERIAL mode. Use -parallel for parallel mode");
        }

        if(!draw_graphics_)
        {
            msg::info("Graphics has been disabled.");
        }
        if(override_bound_)
        {
            msg::info("Bound has been overridden to %d", overridden_bound_);
        }
        return true;
    }

    bool read_circuit(const char *filename, Netlist& netlist)
    {
        std::ifstream fp(filename);
        if(fp.fail() || fp.bad())
        {
            msg::error("Failed to open circuit file '%s'.", filename);
            cmd_help();
            return false;
        }
        msg::info("Reading circuit file '%s'.", filename);
        std::string buf;
        size_t line = 0;
        int block_id;
        std::map<int, std::vector<int>> net_to_block_map;
        int max_net_id = 0;
        while(!fp.eof() && !fp.fail() && !fp.bad())
        {
            std::getline(fp, buf);
            ++line;
            if(buf.empty())
            {
                continue;
            }
            if(buf == "-1")
            {
                
                break;
            }
            std::vector<int> nets;
            read_net(buf, line, block_id, nets);
            netlist.add_block(block_id);
            for(int net_id : nets)
            {
                if(net_id > max_net_id)
                {
                    max_net_id = net_id;
                }
                netlist.block(block_id).add_net(net_id);
                auto it = std::find(net_to_block_map[net_id].begin(), net_to_block_map[net_id].end(), block_id);
                pt_assert(it == net_to_block_map[net_id].end());
                net_to_block_map[net_id].push_back(block_id);
            }
        }

        netlist.resize_nets(max_net_id + 1);
        for(auto net_blocks_pair : net_to_block_map)
        {
            const std::vector<int>& blocks = net_blocks_pair.second;
            for(int iblock : blocks)
            {
                netlist.add_block_to_net(iblock, net_blocks_pair.first);
                for(int jblock : blocks)
                {
                    if(iblock == jblock)
                    {
                        continue;
                    }
                    netlist.connect_blocks(iblock, jblock);
                }
            }
        }

        return true;
    }

    void draw_callback()
    {
        flushinput();
    }

    void setup_graphics(const Netlist& netlist)
    {
        if(!draw_graphics())
        {
            return;
        }
        init_graphics("Partitioning", WHITE);
        set_visible_world(t_bound_box(0, 
            0, 
            1000,
            1000)
        );

        y_scale_ratio = 1000.0 / netlist.blocks().size();
        x_scale_ratio = 1000.0 / (1 << netlist.blocks().size());

        event_loop(NULL, NULL, NULL, draw_callback);
    }

    void draw_node(float x, float y, int color)
    {
        pt_assert(draw_graphics_);
        graphics_mutex_.lock();
        setcolor(color);
        x = x * x_scale_ratio;
        y = y * y_scale_ratio;
        y -= 5;
        fillarc(x, y, 5, 0, 360);
        graphics_mutex_.unlock();
    }

    void cleanup_graphics()
    {
        if(!draw_graphics())
        {
            return;
        }
        event_loop(NULL, NULL, NULL, draw_callback);
    }

    bool draw_graphics()
    {
        return draw_graphics_;
    }

    bool override_bound()
    {
        return override_bound_;
    }

    int overridden_bound()
    {
        return overridden_bound_;
    }

    int parallel()
    {
        return parallel_;
    }

    bool print_placement()
    {
        return print_placement_;
    }

	void die_at(const char *filename, int line)
	{
		const int TRACE_LIMIT = 20;
		void *array[TRACE_LIMIT];
		msg::error("Fatal exit at %s:%d", filename, line);
  		size_t size;

  		// get void*'s for all entries on the stack
  		size = backtrace(array, TRACE_LIMIT);
        msg::error("Begin Stack Trace:-");
  		backtrace_symbols_fd(array, size, STDOUT_FILENO);
        msg::error("End Trace");
		exit(EXIT_FAILURE);
	}
}
