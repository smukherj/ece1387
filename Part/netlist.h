#ifndef NETLIST_H
#define NETLIST_H

#include <vector>
#include <unordered_map>

class Block;

class Netlist
{
public:

	Netlist();
	~Netlist();

	const std::vector<Block>& blocks() const { return m_blocks; }
	const std::vector<int>& net(int net_id) const { return m_nets[net_id]; }
	const std::vector<std::vector<int>> nets() const { return m_nets; }

	Block& block(int id);
	const std::vector<int>& connections(int id) const;

	int connected(int lhs_id, int rhs_id);

	void resize_nets(size_t size);
	void add_block(int id);
	void add_block_to_net(int block_id, int net_id);
	void connect_blocks(int lhs_id, int rhs_id);

private:
	std::vector<Block> m_blocks;
	std::unordered_map<int, std::vector<int>> m_edges;
	std::vector<std::vector<int>> m_nets;
};

#endif