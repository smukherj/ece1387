#ifndef BLOCK_H
#define BLOCK_H

#include <vector>
#include <map>

enum class Partition
{
	LEFT,
	RIGHT,
	OPEN
};

class Block
{
public:

	Block(int id);
	~Block();
	int id() const { return m_id; }
	Partition p() const { return m_p; }
	const std::vector<int> nets() const { return m_nets; }

	void set_p(Partition p) { m_p = p; }
	void add_net(int net_id);
	
private:
	int m_id;
	Partition m_p;
	std::vector<int> m_nets;
};

#endif /* BLOCK_H */