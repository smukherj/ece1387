#include "block.h"
#include "utils.h"
#include <algorithm>

Block::Block(int id)
{
	m_id = id;
	m_p = Partition::OPEN;
}

Block::~Block()
{
}

void Block::add_net(int net_id)
{
	pt_assert(std::find(m_nets.begin(), m_nets.end(), net_id) == m_nets.end());
	m_nets.push_back(net_id);
}
